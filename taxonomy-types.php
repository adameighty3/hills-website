<?php get_header(); ?>

<?php 
    $terma = get_queried_object();  
  //  print_r($terma);
?>

<div class="flexible-content-blocks">
<div class="case-studies-header">

<?php $term = get_queried_object(); //print_r($term);  ?>
<h1><?php echo $terma->name; ?></h1>

</div>

<div class="filters">
    <div class="container">
        <div class="filters">
    <?php $terms = get_terms(
        array(
            'taxonomy' => 'types',
            'hide_empty' => 'false',
        )
        );
    
    
    foreach($terms as $term){ ?>

        <li>

            <a href="/type/<?= $term->slug; ?>">
                <?= $term->name; ?>
            </a>

        </li>

   
    <?php
        }
    ?>
 </div>
    </div>

</div>


<div class="featured_case-studies">

    <div class="container">

    <div class="row">


            <?php 

                $args = array(
                    'post_type' => 'case_studies',
                    'posts_per_page' => 6,
                    'tax_query' => ( array(
                        array(
                            'taxonomy' => 'types',
                            'field' => 'slug',
                            'terms' => $terma->slug,
                            'compare' => '=',
                        ),
                    )
                ),
            );

                $query = new WP_Query($args);

                if($query->have_posts() ):

                    $i == 0;
                    while($query->have_posts() ): $query->the_post();            $i++ ?>
        

                    <?php $terms = get_terms( array(
                                    'taxonomy'   => 'types',
                                    'hide_empty' => false,
                                    'object_ids' => get_the_ID(),
                                    ) );

                        if($i == 1){ echo '<div class="col-md-8">';};
                        if($i == 2){ echo '</div><div class="col-md-4">';};
                        if($i == 4){ echo '</div><div class="col-md-4"> ';};
                        if($i == 6){ echo '</div><div class="col-md-8"> '; };
    
                    ?>

                   

                        <?php $image = get_field('case_study_image'); ?>
                        <div class="case-study">
                        <a href="<?php the_permalink(); ?>">
                        <div class="item" style="background-image:url('<?= $image['url']; ?>');">
            
                            <div class="case-study_content">
                         
                                <div class="border-line"></div>
                                <div class="case-study_content_inner">
                                    <?php foreach($terms as $term){  ?>      
                                    <h3><?php print_r($term->name); ?></h3>
                                    <?php } ?>
                                    <p><?php the_title(); ?></p>
                                </div>
                                    
                            </div> 
                            </a>  
                        </div>
                    </div>

                    

                    

                    <?php

if($i == 6){ echo '</div>'; };
                

                    endwhile;

                    ?>
                    <div class="container-fluid">
                    <div class="pagination">
                        <?php 
                            echo paginate_links( array(
                                'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                                'total'        => $query->max_num_pages,
                                'current'      => max( 1, get_query_var( 'paged' ) ),
                                'format'       => '?paged=%#%',
                                'show_all'     => false,
                                'type'         => 'plain',
                                'end_size'     => 2,
                                'mid_size'     => 1,
                                'prev_next'    => true,
                                'prev_text'    => sprintf( '<i></i> %1$s', __( 'Newer Posts', 'text-domain' ) ),
                                'next_text'    => sprintf( '%1$s <i></i>', __( 'Older Posts', 'text-domain' ) ),
                                'add_args'     => false,
                                'add_fragment' => '',
                            ) );
                        ?>
                    </div>
                    </div>
                    <?php
                    endif;

                    wp_reset_query();

                    ?>

                

            </div>


        </div>

    </div>
                </div>

<?php get_footer(); ?>