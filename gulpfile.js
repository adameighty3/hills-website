var gulp = require('gulp');
var sass = require('gulp-sass')(require('sass'));
var watch = require('gulp-watch');
var rename = require('gulp-rename');
var concatJS = require('gulp-concat');
var concatCSS = require('gulp-concat-css');
var cleanCSS = require ('gulp-clean-css');
var clean = require ('gulp-clean');
var uglify = require ('gulp-uglify');

sass.compiler = require('node-sass');

// CSS Tasks
  //convert sass to css
  gulp.task('sass', function () {
    return gulp.src('./assets/sass/**/*.scss')
      .pipe(sass.sync().on('error', sass.logError))
      .pipe(gulp.dest('./assets/css'));
  });  

  //compile css into one file
  gulp.task('concat-css', function() {
    return gulp.src('./assets/css/**/*.css')
      .pipe(concatCSS('./assets/compiled/main.css'))
      .pipe(gulp.dest('.'));
  });

  //minify css
  gulp.task('minify-css', function() {
    return gulp.src('./assets/compiled/main.css')
      .pipe(cleanCSS({compatibility: 'ie8'}))
      .pipe(gulp.dest('./assets/dist'));
  });
//

// JS Tasks
  //compile js into one file
  gulp.task('concat-js', function() {
    return gulp.src('./assets/js/**/*.js')
      .pipe(concatJS('./assets/compiled/all.js'))
      .pipe(gulp.dest('.'));
  });

  //minify js
  gulp.task('compress-js', function () {
    return gulp.src('./assets/compiled/all.js')
          .pipe(uglify())
          .pipe(gulp.dest('./assets/dist'));
  });
//

gulp.task('compiler', gulp.series('sass','concat-css','concat-js','compress-js','minify-css'));

//watch the scss folder and run all of the above when an scss file is updated
gulp.task('watch', function() {
  gulp.watch('./assets/sass/**/*.scss', gulp.series('sass','concat-css','minify-css','concat-js','compress-js'));
});


gulp.task('default', gulp.series('sass','concat-css','minify-css','concat-js','compress-js','watch'));
