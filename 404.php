<?php get_header(); ?>

<div class="container-fluid page_not_found">
	<div class="row">
		<div class="container">	
			<div class="wrapper">
				<h1>Page Not Found</h1>
				<p>There seems to have been an error.</p>
			</div>
		</div>
	</div>
</div>

<?php get_footer();