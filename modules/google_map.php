<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAyeHZmrpJw3hZtdnu1HAgdK2wuKHoOgR4"></script>
<div class="container-fluid google-map">
<?php
$location = get_sub_field('map');
	if( $location ): ?>
	    <div class="acf-map" data-zoom="14">
	        <div class="marker" data-lat="<?php echo esc_attr($location['lat']); ?>" data-lng="<?php echo esc_attr($location['lng']); ?>"></div>
	    </div>
	<?php endif; ?>
</div>