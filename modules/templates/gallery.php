<div class="container-fluid gallery_block">
	<div class="row">
		<div class="container">
			<h2>Slick Gallery</h2>
		</div>
		<div class="container">
			<div class="multiple-items">
				<?php 
				$images = get_sub_field('gallery');
				if( $images ): ?>
			        <?php foreach( $images as $image ): ?>
			        	<div>
				            <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
			        	</div>
			        <?php endforeach; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>