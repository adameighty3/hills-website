<div class="featured_case-studies">

    <div class="container-fluid">

        <div class="row">


            <?php 

                $args = array(
                    'post_type' => 'case_studies',
                    'posts_per_page' => 1,
                );

                $query = new WP_Query($args);

                if($query->have_posts() ):
                    $i = 0;
                    while($query->have_posts() ): $query->the_post(); 
                    $i = $i+0.25;
                    ?>
                
                    <?php $terms = get_terms( array(
                                    'taxonomy'   => 'types',
                                    'hide_empty' => false,
                                    'object_ids' => get_the_ID(),
                                    ) );
                    ?>

                    <div class="col-md-6 case-study-width wow animate__animated animate__fadeIn" data-wow-delay="<?= $i ?>s">

                        <?php $image = get_field('case_study_image'); ?>

                        <div class="case-study large" style="background-image:url('<?= $image['url']; ?>');">
                        <a href="<?php the_permalink(); ?>">
                            <div class="case-study_content">
                                <div class="border-line"></div>
                                <div class="case-study_content_inner">
                                    <?php foreach($terms as $term){  ?>      
                                    <h3><?php print_r($term->name); ?></h3>
                                    <?php } ?>
                                    <p><?php the_title(); ?></p>
                                </div>
                            </div>
                                    </a>
                        </div>

                        </div>

   

                    <?php

                    endwhile;
                    endif;

                    wp_reset_query();

                    ?>

                    <div class="col-md-6 case-study-width wow animate__animated animate__fadeIn" data-wow-delay="<?= $i; ?>s">

                    <?php 

                    $args = array(
                        'post_type' => 'case_studies',
                        'posts_per_page' => 2,
                        'offset' => 1,
                    );

                    $query = new WP_Query($args);
  
                    if($query->have_posts() ):

                        while($query->have_posts() ): $query->the_post(); ?>
              

                        <div class="case-study small" style="background-image:url('<?= $image['url']; ?>');">
                        <a href="<?php the_permalink(); ?>">
                        <div class="case-study_content">
                                <div class="border-line"></div>
                                <div class="case-study_content_inner">
                                    <?php foreach($terms as $term){  ?>      
                                    <h3><?php print_r($term->name); ?></h3>
                                    <?php } ?>
                                    <p><?php the_title(); ?></p>
                                </div>
                            </div>
                        </a>
   
                        </div>

                        <?php

                        endwhile;
                        endif;

                        wp_reset_query();

                        ?>

            

                    </div>





            </div>


        </div>

    </div>
