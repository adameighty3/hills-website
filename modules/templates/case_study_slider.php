<div class="container-fluid case-study-slider">

    <div class="row">

        <div class="col-12">

    <div class="slicksliders">
        <?php

        if( have_rows('case_study_slider') ):

            while( have_rows('case_study_slider') ) : the_row();

     
        ?>
            <div class="slickslider">

                <?php $image = get_sub_field('image'); ?>


                <?php
                if( have_rows('image_plots') ):

                    while( have_rows('image_plots') ) : the_row();
                ?>
                <div class="plot-point" style="position:relative; margin-top:<?php the_sub_field('x_axis_px'); ?>%; left:<?php the_sub_field('y_axis_px'); ?>%" >
                    <svg xmlns="http://www.w3.org/2000/svg" width="38.282" height="38.282" viewBox="0 0 38.282 38.282">
                        <g id="Group_1577" data-name="Group 1577" transform="translate(11659 2368)">
                            <circle id="Ellipse_94" data-name="Ellipse 94" cx="19" cy="19" r="19" transform="translate(-11659 -2368)" fill="#fff"/>
                            <path id="Icon_awesome-plus-circle" data-name="Icon awesome-plus-circle" d="M19.7.563A19.141,19.141,0,1,0,38.845,19.7,19.138,19.138,0,0,0,19.7.563Zm11.114,21.3a.929.929,0,0,1-.926.926h-7.1v7.1a.929.929,0,0,1-.926.926H17.543a.929.929,0,0,1-.926-.926v-7.1h-7.1a.929.929,0,0,1-.926-.926V17.543a.929.929,0,0,1,.926-.926h7.1v-7.1a.929.929,0,0,1,.926-.926h4.322a.929.929,0,0,1,.926.926v7.1h7.1a.929.929,0,0,1,.926.926Z" transform="translate(-11659.563 -2368.563)" fill="#ee7b00"/>
                        </g>
                    </svg>
                    <div class="point-hover">
                        <h3><?php the_sub_field('title'); ?></h3>
                        <p><?php the_sub_field('content'); ?></p>

                    </div>
                </div>



                <?php


                endwhile;

                else :

                endif;


                ?>

                <img src="<?= $image['url']; ?>" alt="<?= $image['alt']; ?>"/>



            </div>


        <?php


        endwhile;

        else :

        endif;


        ?>

    </div>
        </div>

    </div>


</div>

<!-- 
    Quote from the content area input
-->

<div class="container">

    <div class="grey-quote-box">
  
        <?php the_content(); ?>

    </div>
    <div class="contact-bar">

        <div class="phone bar">

            <a href="<?php the_field('phone_number', 'options'); ?>"><?php the_field('phone_number', 'options'); ?></a>

        </div>

        <div class="email bar">

            <a href="<?php the_field('email_address', 'options'); ?>"><?php the_field('email_address', 'options'); ?></a>

        </div>
    
    </div>
</div>