<div class="anchor">

    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="anchor-point" id="<?= str_replace(' ', '_', get_sub_field('title')); ?>"><?php the_sub_field('title'); ?></h2>
            </div>
        </div>
    </div>

</div>