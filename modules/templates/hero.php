<div class="hero_block">

<?php if(get_sub_field('style') == 'home'){ ?>


	<div class="hero_block-video">

		<?php

		// Load value.
		$iframe = get_sub_field('video_url');

		// Use preg_match to find iframe src.
		preg_match('/src="(.+?)"/', $iframe, $matches);
		$src = $matches[1];

		// Add extra parameters to src and replace HTML.
		$params = array(
			'controls'  => 0,
			'autoplay'  => 1,
			'background' => 0,
			'muted' => 1,
			
		);
		$new_src = add_query_arg($params, $src);
		$iframe = str_replace($src, $new_src, $iframe);

		// Add extra attributes to iframe HTML.
		$attributes = 'frameborder="0"';
		$iframe = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $iframe);

		// Display customized HTML.
		echo $iframe;

		?>

	</div>

	<!-- hero content -->

	<div class="content animate__animated animate__fadeInLeft">
		<div class="container">
			<div class="content-inner">
				<sub><?php the_sub_field('heading'); ?></sub>
				<div class="hero_content">
						
					<h1><?php the_sub_field('content'); ?></h1>
					<a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('link_text'); ?></a>
				</div>
			</div>
		</div>

	</div>

</div>

<?php }else{ ?>

<?php $image = get_sub_field('background_image'); ?>

<div class="hero_block inner" style="background-image:url('<?= $image; ?>');">
	<div class="hero_block-inner">

	<!-- hero content -->

	<div class="content">
		<div class="container">
			<div class="content-inner">
			
				<div class="hero_content wow animate__animated animate__fadeIn" data-wow-delay="0.3s">
						
					<h1><?php the_sub_field('heading'); ?>
				
					<svg xmlns="http://www.w3.org/2000/svg" width="29.116" height="21.684" viewBox="0 0 29.116 21.684">
					<g id="Group_1556" data-name="Group 1556" transform="translate(-557.426 -589.724)">
						<path id="Icon_ionic-ios-arrow-forward" data-name="Icon ionic-ios-arrow-forward" d="M19.907,17.034,11.7,8.835a1.543,1.543,0,0,1,0-2.189,1.563,1.563,0,0,1,2.2,0l9.3,9.29a1.547,1.547,0,0,1,.045,2.137L13.9,27.429a1.55,1.55,0,0,1-2.2-2.189Z" transform="translate(546.179 583.527)" fill="#ee7b00"/>
						<path id="Icon_ionic-ios-arrow-forward-2" data-name="Icon ionic-ios-arrow-forward" d="M19.907,17.034,11.7,8.835a1.543,1.543,0,0,1,0-2.189,1.563,1.563,0,0,1,2.2,0l9.3,9.29a1.547,1.547,0,0,1,.045,2.137L13.9,27.429a1.55,1.55,0,0,1-2.2-2.189Z" transform="translate(562.897 583.527)" fill="#ee7b00"/>
					</g>
					</svg>

				</h1>
					<a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('link_text'); ?></a>
				</div>
				<sub class="wow animate__animated animate__fadeIn" data-wow-delay="0.55s"><?php the_sub_field('content'); ?></sub>
			</div>
		</div>

	</div>


	</div>
</div>

<?php } ?>