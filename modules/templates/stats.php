<div class="stats">

    <div class="container">


        <div class="row">

            <?php

            if( have_rows('stats') ):
                $i=0;

                while( have_rows('stats') ) : the_row();
                $i = $i+0.25; 
            ?>

            <div class="col-md-3  wow animate__animated animate__fadeIn" data-wow-delay="<?= $i; ?>s">
                <div class="wrapper">
                <div class="circles">

                    <li class="data-one" data-dimension="250" data-text="<?php the_sub_field('stat_percentage'); ?>%" data-info="" data-width="30" data-fontsize="38" data-percent="<?php the_sub_field('stat_percentage'); ?>" data-fgcolor="#464646" data-bgcolor="#eee"></li>
                    <h3><?php the_sub_field('stat_title'); ?></h3>
                </div>
            </div>
            </div>

            <?php

                endwhile;


            else :

            endif;
            ?>

        </div>


    </div>

</div>