<div class="two_column_content">

    <div class="container">

        <div class="row">


            <div class="col-md-8 case-study-width wow animate__animated animate__fadeIn" data-wow-delay="0.3s">

                <?php the_sub_field('wysiwyg'); ?>

            </diV>

            <div class="col-md-4">

                <?php if(get_sub_field('right_column_content') == 'casestudies' ){

                ?>

                
                    <div class="preview-box">

                    <?php 

                        $args = array(
                            'post_type' => 'case_studies',
                            'posts_per_page' => 1,
                            // 'meta_key' => 'set_featured',
                           //  'meta_value' => 'set',  
                            'meta_query' => array(
                                array(
                                   'key' => 'set_featured',
                                   'value' => 'set',
                                   'compare' => 'LIKE',     
                                ),
                           )
                        );

                        $query = new WP_Query($args);

                        if($query->have_posts() ):
                            while($query->have_posts() ): $query->the_post(); ?>

                            <?php the_content(); ?>

                            <a class="button button-primary" href="<?php the_permalink(); ?>">
                                VIEW CASE STUDY >
                            </a>

                        <?php

                            endwhile;
                            endif;

                            wp_reset_query();

                        ?>


                    </div>


                <?php
                }elseif(get_sub_field('right_column_content') == 'single' ){ ?>

            <div class="provided">
                <h2>What our team provided</h2>
            <?php       
                if( have_rows('what_we_provided') ):

                    while( have_rows('what_we_provided') ) : the_row();
                ?>
                <div class="list-item">
                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25">
                <g id="Group_1858" data-name="Group 1858" transform="translate(-1137 -627.468)">
                    <path id="Icon_material-lightbulb-outline" data-name="Icon material-lightbulb-outline" d="M10.869,19a.845.845,0,0,0,.842.842h3.369A.845.845,0,0,0,15.921,19v-.842H10.869ZM13.395,3a5.892,5.892,0,0,0-3.369,10.729v1.9a.845.845,0,0,0,.842.842h5.053a.845.845,0,0,0,.842-.842v-1.9A5.892,5.892,0,0,0,13.395,3Zm2.4,9.348-.716.505V14.79H11.711V12.853l-.716-.505a4.211,4.211,0,1,1,4.8,0Z" transform="translate(1136.221 628.38)" fill="#ee7b00"/>
                    <g id="Ellipse_62" data-name="Ellipse 62" transform="translate(1137 627.468)" fill="none" stroke="#484847" stroke-width="2">
                    <circle cx="12.5" cy="12.5" r="12.5" stroke="none"/>
                    <circle cx="12.5" cy="12.5" r="11.5" fill="none"/>
                    </g>
                </g>
                </svg>

                    <?php the_sub_field('item'); ?>
                </div>
                <?php

                endwhile;
                endif;

                wp_reset_query();

                ?>
                </div>
                <!-- Need Help Form -->

				<div class="need_help-form">

				<h2>Need help?</h2>
				<p>Feel free to fill out our form below and a member of our team will be in touch.</p>

				<?php echo do_shortcode('[forminator_form id="165"]'); ?>
				</div>

                <?php

                

                }elseif(get_sub_field('right_column_content') == 'casestudy' ){

                ?>

                    <div class="case-study-detail">
                        <label>Location:</label><span><?php the_sub_field('location'); ?></span>
                    </div>
                    <div class="case-study-detail">
                        <label>Year:</label><span><?php the_sub_field('year'); ?></span>
                    </div>
                    <div class="case-study-detail">
                        <label>Solution:</label><span><?php the_sub_field('solution'); ?></span>
                    </div>
                    <div class="case-study-detail">
                        <label>Sector:</label><span><?php the_sub_field('sector'); ?></span>
                    </div>

                <?php

                }else{

                    ?>

                        <ul>

                            <li> <img src="<?= get_template_directory_uri(); ?>/assets/img/phone.png"/> <a href="tel:<?php the_field('phone_number', 'options'); ?>"><?php the_field('phone_number', 'options'); ?></a> </li>
                            <li> <img src="<?= get_template_directory_uri(); ?>/assets/img/envelope.png"/><a href="mailto:<?php the_field('email_address', 'options'); ?>"><?php the_field('email_address', 'options'); ?></a> </li>
                            <li> <img src="<?= get_template_directory_uri(); ?>/assets/img/mappin.png"/>  <h3>Location</h3> <div class="address_block"><?php $count = count(get_field('address','options'));
								if(have_rows('address', 'options')) : $n=1; while(have_rows('address', 'options')) : the_row(); ?>
									<?php
									if($n < $count) { echo '<span>'. get_sub_field('line') . ',</span> '; }
									else { the_sub_field('line'); }
								$n++; endwhile; endif; ?></div></li>

                        </ul>

                    <?php

                }
    
                ?>

            </div>


        </div>


    </div>

</div>