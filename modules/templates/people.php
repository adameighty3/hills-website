<div class="people">

    <div class="container">

        <div class="row">

            <?php

            if( have_rows('people') ):
                $i = 0;
                $s = 0.5;

                while( have_rows('people') ) : the_row();
                $i = $i+0.25; 
            ?>
            <div class="col-md-4  wow animate__animated animate__fadeIn" data-wow-delay="<?= $i; ?>s">

                <?php

                    $image = get_sub_field('image');
               

                ?>
                <div class="person" style="background-image:url('<?= $image['url']; ?>');">

                    <div class="details">

                        <h3><?php the_sub_field('name'); ?></h3>
                        <sub><?php the_sub_field('position'); ?></sub>

                    </div>
                    <div class="line"></div>

                </div>

            </div>

            <?php

            endwhile;


            else :

            endif;
            ?>

        </div>


    </div>


</div>