<div class="listings spacing">

    <div class="container">


        <div class="row">


            <?php


        
            if( have_rows('listings') ):

                $i = 0.5;
                $s = 0.5;
                while( have_rows('listings') ) : the_row();
                $i = $i+0.25;
                ?>



                <div class="col-md-4">


                    <?php 

                        $image = get_sub_field('image');

                    ?>

                    <div class="column_container wow animate__animated animate__fadeIn" data-wow-delay="<?= $i-$s; ?>s">

                        <div class="column_container-image">

                            <img src="<?= $image['url']; ?>" alt="<?= $image['alt']; ?>" />

                        </div>
                      
                        <div class="column_container-content">

                            <h2><?php the_sub_field('title'); ?></h2>
                            <p><?php the_sub_field('content'); ?></p>
                            <a href="<?php the_sub_field('link'); ?>" class="button button-primary"><?php the_sub_field('link_text'); ?></a>

                        </div>

                    </div>


                </div>



                <?php


                endwhile;

            else :

            endif;


            ?>


        </div>

    </div>

</div>
