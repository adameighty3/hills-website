<div class="stand_out-image">

    <div class="container">

        <div class="row">

            <div class="col-md-6 stand-out-col wow animate__animated animate__fadeIn" data-wow-delay="0s">

                <div class="stand_out-content" style="background-image:url('<?= get_template_directory_uri(); ?>/assets/img/bg.png');">

                    <sub><?php the_sub_field('sub_title'); ?> <span>>></span></sub>
                    <h2><?php the_sub_field('title'); ?></h2> 
                    <p><?php the_sub_field('content'); ?></p>

                </div>

            </div>
            <div class="col-md-6 large-image_barrier wow animate__animated animate__fadeIn" data-wow-delay="0.25s">

                <?php $image = get_sub_field('image'); ?>

                <img class="large-image" src="<?= $image['url']; ?>" alt="<?= $image['alt']; ?>"/>

            </div>


        </div>

    </div>

</div>