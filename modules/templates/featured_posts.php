<div class="container">

<?php

$var = get_sub_field('set_amount_of_posts_to_show');

if($var == 2){
    $cols = '6';
}elseif($var == 3){
    $cols = '4';
}else{
    $cols = '3';
}

?>

    <div class="row">
        <?php $args = array('posts_per_page' => $var);
              $posts = new WP_Query($args);
              if($posts->have_posts()) :
                $i = 0;
                $s = 0.5;
                while($posts->have_posts()) :
                    $i = $i+0.25;
                   // print_r($i);
                    $posts->the_post();
        ?>
       
        <div class="col-md-<?= $cols; ?> wow animate__animated animate__fadeIn" data-wow-delay="<?= $i; ?>s">
             
          
            <?php

                $thumb_id = get_post_thumbnail_id();
                $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
                $thumb_url = $thumb_url_array[0];

            ?>

            <div class="post-container" style="background-image:url('<?= $thumb_url ?>');">

                <div class="post-container-inner">
                <span>
                    <svg xmlns="http://www.w3.org/2000/svg" width="15" height="16.5" viewBox="0 0 15 16.5">
                        <g id="Icon_feather-calendar" data-name="Icon feather-calendar" transform="translate(-3 -1.5)">
                            <path id="Path_831" data-name="Path 831" d="M5.25,4.5h10.5A2.253,2.253,0,0,1,18,6.75v10.5a2.253,2.253,0,0,1-2.25,2.25H5.25A2.253,2.253,0,0,1,3,17.25V6.75A2.253,2.253,0,0,1,5.25,4.5ZM15.75,18a.751.751,0,0,0,.75-.75V6.75A.751.751,0,0,0,15.75,6H5.25a.751.751,0,0,0-.75.75v10.5a.751.751,0,0,0,.75.75Z" transform="translate(0 -1.5)" fill="#ee7b00"/>
                            <path id="Path_832" data-name="Path 832" d="M23.25,6a.75.75,0,0,1-.75-.75v-3a.75.75,0,0,1,1.5,0v3A.75.75,0,0,1,23.25,6Z" transform="translate(-9.75)" fill="#ee7b00"/>
                            <path id="Path_833" data-name="Path 833" d="M11.25,6a.75.75,0,0,1-.75-.75v-3a.75.75,0,0,1,1.5,0v3A.75.75,0,0,1,11.25,6Z" transform="translate(-3.75)" fill="#ee7b00"/>
                            <path id="Path_834" data-name="Path 834" d="M17.25,15H3.75a.75.75,0,0,1,0-1.5h13.5a.75.75,0,0,1,0,1.5Z" transform="translate(0 -6)" fill="#ee7b00"/>
                        </g>
                    </svg>
                    <?php echo get_the_date('dS F Y'); ?>
                </span>
                    <h2><?php the_title(); ?></h2>
                    <span class="line"></span>
                    <?php the_field('sub_content'); ?>
                    <a href="<?php the_permalink(); ?>">READ MORE 
                    <svg xmlns="http://www.w3.org/2000/svg" width="18.506" height="12.343" viewBox="0 0 18.506 12.343">
                                <path id="Icon_ionic-ios-arrow-round-forward" data-name="Icon ionic-ios-arrow-round-forward" d="M19.677,11.488a.84.84,0,0,0-.006,1.183l3.908,3.915H8.7a.836.836,0,0,0,0,1.671H23.572l-3.908,3.915a.846.846,0,0,0,.006,1.183.832.832,0,0,0,1.176-.006l5.3-5.335h0a.938.938,0,0,0,.174-.264.8.8,0,0,0,.064-.321.838.838,0,0,0-.238-.585l-5.3-5.335A.819.819,0,0,0,19.677,11.488Z" transform="translate(-7.875 -11.252)" fill="#ee7b00"/>
                            </svg>
                </a>

                </div>

            </div>

        </div>

        <?php endwhile;
        
                else:

                endif;
                wp_reset_query();
        ?>


</div>