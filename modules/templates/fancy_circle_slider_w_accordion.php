<?php echo do_shortcode('[e3c_circle]'); ?>

<svg id="Component_21_1" data-name="Component 21 – 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="1808" height="1073" viewBox="0 0 1808 1073">
  <defs>
    <clipPath id="clip-path">
      <path id="Rectangle_1583" data-name="Rectangle 1583" d="M0,0H1151a0,0,0,0,1,0,0V773a300,300,0,0,1-300,300H0a0,0,0,0,1,0,0V0A0,0,0,0,1,0,0Z" transform="translate(657 993)" fill="#484847"/>
    </clipPath>
    <clipPath id="clip-path-2">
      <path id="Path_912" data-name="Path 912" d="M4119.257,0a3163.728,3163.728,0,0,0,340.338,8.715V.241Q4374.462,2.348,4289.3,0Z" transform="translate(-4119.257 -0.001)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-3">
      <path id="Path_913" data-name="Path 913" d="M3579.595,0q24.769,3.692,49.8,6.949c163.043,21.2,331.624,28.363,498.958,23.2V21.828C3968.722,26.368,3808.068,19.6,3652.45,0Z" transform="translate(-3579.595 -0.001)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-4">
      <path id="Path_914" data-name="Path 914" d="M3200.16,0c69.608,13.732,141.244,24.828,214.177,33.245,157.95,18.229,320.189,23.83,481.118,18.286V43.373c-163.075,5.234-327.39-1.078-486.935-20.495Q3331.216,13.47,3256.259,0Z" transform="translate(-3200.16 -0.001)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-5">
      <path id="Path_915" data-name="Path 915" d="M2882.172,0C2994.617,26.8,3113.6,46.44,3235.623,58.9c153.112,15.633,309.562,19.866,464.65,13.969v-8c-157.29,5.618-315.885.732-470.757-16C3126.7,37.746,3026.206,21.446,2930.151,0Z" transform="translate(-2882.172 -0.001)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-6">
      <path id="Path_916" data-name="Path 916" d="M2598.931,0c148.157,40.821,309.726,68.736,476.036,83.795,149.2,13.509,301.035,16.585,451.454,10.337V86.295c-151.907,5.971-305.172,2.353-455.536-12.008C2921.651,60.034,2776.546,35.27,2641.88,0Z" transform="translate(-2598.931 -0.001)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-7">
      <path id="Path_917" data-name="Path 917" d="M2338.229,0c179.472,55.642,381.484,91.722,589.715,108.3,145.285,11.563,292.592,13.58,438.458,7.015v-7.671c-147.539,6.32-296.479,3.793-443.16-8.6C2731.355,82.836,2545.128,49.839,2377.693,0Z" transform="translate(-2338.229 -0.001)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-8">
      <path id="Path_918" data-name="Path 918" d="M2093.245,0c206.83,70.887,447.427,114.98,695.238,132.274a3633.743,3633.743,0,0,0,427.548,4.119v-7.5a3597.731,3597.731,0,0,1-430.831-5.464C2552.488,106.211,2326.649,65.08,2130.135,0Z" transform="translate(-2093.245 -0.001)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-9">
      <path id="Path_919" data-name="Path 919" d="M1859.627,0q28.714,10.767,58.431,20.843c219.458,74.35,475.792,119.244,738.158,135.086a3728.311,3728.311,0,0,0,416.422,1.442v-7.324a3693.468,3693.468,0,0,1-420.409-2.724C2387.96,130.265,2130.24,83.348,1911.85,6.232Q1903.122,3.15,1894.489,0Z" transform="translate(-1859.627 -0.001)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-10">
      <path id="Path_920" data-name="Path 920" d="M1635.149,0Q1709,29.866,1789.781,55.052c222.609,69.415,477.932,110.622,738.2,124.112a3820.858,3820.858,0,0,0,406.871-.93v-7.147a3787.152,3787.152,0,0,1-409.741-.185C2264.17,156.423,2008.6,113.763,1787.286,42.459Q1725.832,22.659,1668.363,0Z" transform="translate(-1635.149 -0.001)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-11">
      <path id="Path_921" data-name="Path 921" d="M1417.212,0c76.552,33.145,159.625,62.5,247.292,87.784,226.148,65.232,481.657,103.181,741.187,114.382a3912.3,3912.3,0,0,0,395.393-3.192v-6.97a3880.61,3880.61,0,0,1-398.909,2.146C2142.461,182,1887.1,142.775,1662.376,75.94,1587.279,53.6,1515.87,28.225,1449.194,0Z" transform="translate(-1417.212 -0.001)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-12">
      <path id="Path_922" data-name="Path 922" d="M1205.28,0c100.714,46.345,213.491,86.071,333.954,118.506,230.244,61.993,487.483,97.246,747.84,106.319,128.274,4.47,256.829,2.552,383.927-5.242V212.79c-128.494,7.65-258.438,9.253-388,4.273-260.18-10-516.9-46.4-745.623-109.779C1429.3,77.34,1327.67,41.42,1235.889,0Z" transform="translate(-1205.28 -0.001)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-13">
      <path id="Path_923" data-name="Path 923" d="M997.912,0c122.319,59.471,263.576,109.123,415.725,147.634,234.924,59.462,495.057,92.432,757.517,99.5,124.611,3.358,249.3.846,372.564-7.086V233.44c-124.323,7.785-250.06,10.02-375.634,6.241-261.635-7.874-520.612-41.724-753.769-102.124C1273.408,101.055,1142.293,54.847,1027.529,0Z" transform="translate(-997.912 -0.001)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-14">
      <path id="Path_924" data-name="Path 924" d="M794.351,0c142.42,72.823,311.777,132.162,495.506,175.986,239.972,57.239,503.635,88.08,768.9,93.151,120.522,2.3,240.961-.745,360.02-8.758V253.94c-120.258,7.894-241.893,10.693-363.55,7.994-264.251-5.862-526.6-37.542-764.724-95.618C1117.874,124.215,958.423,68.173,823.016,0Z" transform="translate(-794.351 -0.001)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-15">
      <path id="Path_925" data-name="Path 925" d="M594.125,0c160.705,86.091,357.162,154.727,571.42,203.153,245.475,55.481,513.488,84.477,782.412,87.628,116.552,1.365,232.9-2.155,347.917-10.222V274.3c-115.9,7.946-233.124,11.247-350.505,9.549-267.479-3.869-533.789-33.482-777.216-89.532C964.015,147.316,776.587,81.714,621.893,0Z" transform="translate(-594.125 -0.001)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-16">
      <path id="Path_926" data-name="Path 926" d="M397.092,0q15.308,8.56,31.066,16.913C601.8,108.922,815.005,181.172,1047.212,230.67c250.916,53.486,523.132,80.386,795.526,81.431,111.352.426,222.4-3.5,332.2-11.508v-6.1c-111.334,7.947-223.926,11.686-336.791,10.916-271.217-1.852-541.968-29.558-790.979-83.8C815.425,171.14,603.255,97.664,431.892,4.337q-3.968-2.161-7.9-4.336Z" transform="translate(-397.092 -0.001)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-17">
      <path id="Path_927" data-name="Path 927" d="M202.938,0q52.639,30.608,110.7,58.739C495.759,146.992,714.356,215.428,950.11,261.464c254.886,49.771,529.04,73.352,802.335,71.5,101.721-.69,203.054-4.927,303.319-12.5v-5.92c-103.2,7.648-207.513,11.743-312.19,12.053-273.089.806-546.66-23.979-800.263-75.081C708.823,204.263,492.071,134.5,312.873,44.944Q269.392,23.215,229.031,0Z" transform="translate(-202.938 -0.001)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-18">
      <path id="Path_928" data-name="Path 928" d="M11.563,0c58.773,35.423,122.9,68.626,191.688,99.28,189.745,84.56,413.18,149.3,652.073,192,258.422,46.19,534.285,66.58,808.3,61.942,92.142-1.56,183.861-5.964,274.678-13.059v-5.745c-93.693,7.191-188.33,11.5-283.38,12.769-273.891,3.646-549.281-17.892-806.6-65.355C610.494,237.964,388.707,171.939,201.528,86.107A1720.477,1720.477,0,0,1,36.87,0Z" transform="translate(-11.563 -0.001)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-19">
      <path id="Path_929" data-name="Path 929" d="M0,81.936c63.418,35.23,132.129,68.088,205.4,98.247,196.619,80.935,424.359,142.109,666.037,181.589,261.6,42.734,538.9,60.044,813.486,52.742q123.926-3.3,246.282-13.2v-5.569q-125.828,9.977-253.623,13.02c-274.264,6.415-550.947-11.851-811.427-55.645-240.51-40.438-466.656-102.608-661-184.527C131.731,137.644,63.084,103.928,0,67.791Z" transform="translate(0 -67.791)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-20">
      <path id="Path_930" data-name="Path 930" d="M0,188.267c68.02,34.92,141.245,67.318,218.928,96.873C421.69,362.282,653.109,419.792,896.976,456.012c264.247,39.247,542.505,53.468,817.248,43.564q109.153-3.935,216.977-12.907v-5.407q-111.85,9.14-225.33,12.9c-274.766,9.015-552.763-6.241-816.215-46.654-243.1-37.292-473.3-95.984-674.062-174.34C138.939,243.249,66.823,210.506,0,175.267Z" transform="translate(0 -175.267)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-21">
      <path id="Path_931" data-name="Path 931" d="M0,288.7c72.389,34.618,149.911,66.572,231.8,95.543,208.322,73.7,443.178,127.887,689.2,161.124,266.737,36.036,546.063,47.424,821.084,35.054q95.1-4.278,189.115-12.312v-5.24q-97.987,8.24-197.295,12.424c-275.113,11.524-554.26-.845-820.337-38-245.388-34.263-479.195-89.589-685.757-164.469C147.219,343.614,71.022,311.457,0,276.677Z" transform="translate(0 -276.677)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-22">
      <path id="Path_932" data-name="Path 932" d="M0,384.278c77.228,34.586,159.589,66.313,246.178,94.865C459.5,549.487,697.45,600.477,945.349,630.829c268.97,32.933,549.129,41.58,824.333,26.86q81.185-4.343,161.518-11.383v-5.073q-84.258,7.275-169.526,11.6c-275.353,13.915-555.416,4.343-823.845-29.661-247.389-31.338-484.437-83.417-696.223-154.908C156.519,439.542,75.692,407.7,0,373.064Z" transform="translate(0 -373.064)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-23">
      <path id="Path_933" data-name="Path 933" d="M0,475.533C82.413,510.3,170.033,541.98,261.882,570.261,479.743,637.344,720.42,685.229,969.964,712.816c270.911,29.948,551.8,35.956,827.046,18.966q67.41-4.161,134.192-10.153v-4.914q-70.645,6.254-142.028,10.471c-275.447,16.236-556.314,9.333-826.794-21.633C713.234,677.029,473.316,628.09,256.8,559.9,166.653,531.506,80.737,499.785,0,465.057Z" transform="translate(0 -465.057)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-24">
      <path id="Path_934" data-name="Path 934" d="M0,563.06c87.95,35.092,181.229,66.846,278.765,94.933C500.705,721.908,743.837,766.807,994.8,791.714c272.638,27.058,554.061,30.525,829.264,11.363q53.778-3.745,107.138-8.637v-4.758q-57.152,5.18-114.8,9.046c-275.443,18.45-556.934,14.138-829.229-13.9-250.663-25.811-493.162-71.711-713.933-136.7C177.564,619.962,86.14,588.223,0,553.233Z" transform="translate(0 -553.233)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-25">
      <path id="Path_935" data-name="Path 935" d="M0,647.325c93.069,35.251,191.545,66.931,294.271,94.711C520.015,803.084,765.553,845.257,1018,867.705c274.354,24.4,556.445,25.492,831.775,4.28q40.839-3.146,81.427-6.948v-4.6q-43.777,4.055-87.853,7.353c-275.347,20.578-557.306,18.758-831.2-6.45C760.174,838.147,515.394,795.175,290.772,733.3,189.184,705.313,91.876,673.468,0,638.094Z" transform="translate(0 -638.094)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-26">
      <path id="Path_936" data-name="Path 936" d="M0,728.65c99.2,35.776,204.01,67.662,313.131,95.312,229.1,58.049,476.542,97.425,730.037,117.383,275.648,21.7,558.04,20.436,833.126-2.794q27.514-2.323,54.906-4.941v-4.446q-30.517,2.886-61.182,5.41c-275.153,22.625-557.458,23.206-832.729.728-253.1-20.667-499.912-60.807-728.006-119.665C201.426,787.8,97.908,755.791,0,719.942Z" transform="translate(0 -719.942)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-27">
      <path id="Path_937" data-name="Path 937" d="M0,807.339c105.59,36.354,217.009,68.464,332.814,95.979,232.106,55.147,481.258,91.816,735.627,109.37a5244.632,5244.632,0,0,0,834.1-9.61q14.348-1.315,28.661-2.71v-4.293q-17.369,1.675-34.788,3.234c-274.878,24.592-557.4,27.487-833.867,7.646C808.493,988.722,559.888,951.317,328.66,895.39,214.233,867.714,104.206,835.5,0,799.117Z" transform="translate(0 -799.117)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-28">
      <path id="Path_938" data-name="Path 938" d="M0,883.62c111.472,36.726,228.935,68.892,350.821,96.13,234.977,52.512,485.845,86.714,741.209,102.052a5327.824,5327.824,0,0,0,835.461-15.977q1.855-.182,3.709-.368v-4.148q-4.845.476-9.694.944a5308.805,5308.805,0,0,1-835.383,14.106C831.012,1060.376,580.6,1025.46,346.4,972.194,225.993,944.81,110.008,912.577,0,875.855Z" transform="translate(0 -875.855)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-29">
      <path id="Path_939" data-name="Path 939" d="M0,957.691c118.3,37.367,242.847,69.766,371.887,96.818,237.424,49.773,489.607,81.448,745.557,94.538a5408.666,5408.666,0,0,0,813.758-20.065v-4a5389.276,5389.276,0,0,1-819.657,18.908c-255.769-13.715-507.562-46.067-744.321-96.568C239.739,1020.126,116.759,987.668,0,950.328Z" transform="translate(0 -950.328)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-30">
      <path id="Path_940" data-name="Path 940" d="M0,1029.731c124.566,37.8,255.579,70.266,391.138,97,239.8,47.286,493.339,76.643,750.019,87.666a5486.018,5486.018,0,0,0,790.044-23.3v-3.861a5468.772,5468.772,0,0,1-795.935,22.269c-256.547-11.611-509.794-41.625-749-89.62C252.358,1093.013,122.971,1060.515,0,1022.77Z" transform="translate(0 -1022.77)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-31">
      <path id="Path_941" data-name="Path 941" d="M0,1099.883c131.01,38.25,268.682,70.766,410.916,97.141,241.951,44.865,496.7,71.984,753.99,81a5560.791,5560.791,0,0,0,766.3-26.184v-3.721a5544.155,5544.155,0,0,1-770.458,25.33c-256.952-9.489-511.235-37.109-752.516-82.495C266.856,1164.359,130.083,1131.663,0,1093.271Z" transform="translate(0 -1093.271)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-32">
      <path id="Path_942" data-name="Path 942" d="M0,1168.285c137.6,38.71,282.087,71.268,431.163,97.254,243.9,42.515,499.722,67.457,757.51,74.528a5633.115,5633.115,0,0,0,742.528-28.727v-3.586a5618.171,5618.171,0,0,1-746.684,27.968c-257.507-7.526-512.91-32.955-756.219-75.967-148.147-26.19-291.675-58.911-428.3-97.74Z" transform="translate(0 -1162.014)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-33">
      <path id="Path_943" data-name="Path 943" d="M0,1235.047c145.052,39.37,297.288,72.078,454.163,97.688,245.488,40.076,502.016,62.8,759.947,67.9a5703.473,5703.473,0,0,0,717.091-30.985V1366.2a5689,5689,0,0,1-722.9,30.281c-257.953-5.622-514.349-28.924-759.492-69.632-155.066-25.751-305.5-58.48-448.805-97.753Z" transform="translate(0 -1229.092)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-34">
      <path id="Path_944" data-name="Path 944" d="M0,1300.258c151.181,39.637,309.709,72.216,472.857,97.292,247.265,38,504.835,58.786,763.333,62.152a5770.279,5770.279,0,0,0,695.011-32.867v-3.327a5757.129,5757.129,0,0,1-699.135,32.283c-258.3-3.778-515.569-25.013-762.368-63.484C307.581,1367.037,150.113,1334.319,0,1294.6Z" transform="translate(0 -1294.601)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-35">
      <path id="Path_945" data-name="Path 945" d="M0,1364.033c158.138,40.1,323.867,72.645,494.214,97.195,248.7,35.842,506.976,54.637,765.7,56.235a5835.33,5835.33,0,0,0,671.292-34.5v-3.2a5823.155,5823.155,0,0,1-675.39,34c-258.567-1.995-516.58-21.223-764.877-57.514C321.661,1431.5,157.034,1398.822,0,1358.661Z" transform="translate(0 -1358.661)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-36">
      <path id="Path_946" data-name="Path 946" d="M0,1426.433c165.191,40.545,338.219,73.036,515.854,97.009,249.994,33.739,508.87,50.61,767.731,50.492a5899.348,5899.348,0,0,0,647.616-35.856V1535a5886.956,5886.956,0,0,1-651.685,35.422c-258.751-.265-517.4-17.549-767.048-51.721C335.948,1494.54,164.056,1461.926,0,1421.328Z" transform="translate(0 -1421.328)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-37">
      <path id="Path_947" data-name="Path 947" d="M0,1487.534c172.331,40.986,352.749,73.389,537.744,96.73,251.159,31.69,510.524,46.691,769.463,44.915a5959.887,5959.887,0,0,0,623.994-36.957v-2.954a5949.659,5949.659,0,0,1-629.636,36.6c-259.1,1.345-518.53-14.152-769.57-46.394C348.935,1555.958,170.463,1523.537,0,1482.681Z" transform="translate(0 -1482.681)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-38">
      <path id="Path_948" data-name="Path 948" d="M0,1547.417c178.847,41.25,365.967,73.443,557.6,96.087,252.384,29.823,512.384,43.1,771.575,39.783a6019.035,6019.035,0,0,0,602.024-37.825v-2.838a6009.147,6009.147,0,0,1-606.027,37.519c-259.144,2.973-519.006-10.691-771.135-40.918C363.58,1616.39,177.658,1584.09,0,1542.812Z" transform="translate(0 -1542.812)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-39">
      <path id="Path_949" data-name="Path 949" d="M0,1606.122c186.132,41.669,380.8,73.706,579.914,95.624,253.3,27.884,513.63,39.4,772.761,34.512,194.714-3.672,388.646-16.609,578.526-38.45v-2.724c-191.185,21.9-386.467,34.762-582.49,38.2-259.122,4.547-519.332-7.329-772.43-35.6C378.361,1675.578,184.924,1643.427,0,1601.74Z" transform="translate(0 -1601.74)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-40">
      <path id="Path_950" data-name="Path 950" d="M0,1663.667c132.706,28.866,269.552,52.947,409.146,72.025,250.761,34.271,510.139,52.407,770,54.2a6171.037,6171.037,0,0,0,752.055-40.6v-2.612a6155.194,6155.194,0,0,1-716.46,40.354c-259.874-.478-519.6-17.317-771.005-50.351C292.12,1716.767,143.611,1690.953,0,1659.512Z" transform="translate(0 -1659.512)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-41">
      <path id="Path_951" data-name="Path 951" d="M0,1720.19q69,14.6,139.391,27.484c245.842,45,503.075,74.116,763.527,86.828a6285.285,6285.285,0,0,0,828.342-14.417q100.627-8.446,199.94-20.133v-2.5q-78.84,9.237-158.6,16.446a6271.092,6271.092,0,0,1-829.857,19.37c-260.521-11.22-518.2-38.853-764.843-82.423q-90.013-15.9-177.9-34.613Z" transform="translate(0 -1716.236)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-42">
      <path id="Path_952" data-name="Path 952" d="M0,1775.664c193.522,39.781,395.056,69.706,600.536,89.32,268.9,25.667,544.368,33.681,817.412,24.065,172.948-6.091,344.69-19.294,513.253-39.276v-2.39c-153.426,18.132-309.5,30.637-466.726,37.25-273.412,11.5-549.737,5.285-819.915-18.729-220.767-19.623-437.3-51.122-644.56-93.983Z" transform="translate(0 -1771.921)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-43">
      <path id="Path_953" data-name="Path 953" d="M0,1830.112q133,26.626,270.494,47.1c154.35,22.978,312.167,40.006,471.645,51.01h43.9c-157.221-9.844-312.981-25.536-465.544-47.023Q157.446,1858.235,0,1826.547Z" transform="translate(0 -1826.547)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-44">
      <path id="Path_954" data-name="Path 954" d="M4093.773,2039.55H4130.9q157.8-10.82,313.049-29.321v-2.31c-115.5,13.726-232.437,24.293-350.178,31.63" transform="translate(-4093.773 -2007.92)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-45">
      <path id="Path_955" data-name="Path 955" d="M0,1883.722c171.707,33.451,349.192,59.377,529.8,77.528h29.464c-190.806-18.385-378.276-45.438-559.268-80.9Z" transform="translate(0 -1880.355)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-46">
      <path id="Path_956" data-name="Path 956" d="M0,1936.345q129.586,24.61,263.092,43.571,51.429,7.3,103.306,13.737h23.421q-31.431-3.8-62.716-7.917Q160.89,1963.844,0,1933.144Z" transform="translate(0 -1933.144)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-47">
      <path id="Path_957" data-name="Path 957" d="M0,1988.149q112.544,20.807,227.91,37.415h19.774Q122.312,2007.843,0,1985.134Z" transform="translate(0 -1985.134)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-48">
      <path id="Path_958" data-name="Path 958" d="M0,2039.027q51.72,9.353,104.055,17.844H121.51Q60.353,2047.1,0,2036.138Z" transform="translate(0 -2036.138)" fill="rgba(255,255,255,0.03)"/>
    </clipPath>
    <clipPath id="clip-path-49">
      <rect id="Rectangle_1518" data-name="Rectangle 1518" width="81.72" height="65.594" fill="#e2e2e2"/>
    </clipPath>
    <clipPath id="clip-path-50">
      <rect id="Rectangle_1521" data-name="Rectangle 1521" width="58.46" height="63.775" fill="#e2e2e2"/>
    </clipPath>
    <clipPath id="clip-path-51">
      <rect id="Rectangle_1526" data-name="Rectangle 1526" width="94.462" height="144.968" fill="#ee7b00"/>
    </clipPath>
  </defs>
  <g id="Group_1805" data-name="Group 1805" transform="translate(0 -993)">
    <path id="Rectangle_1525" data-name="Rectangle 1525" d="M0,0H1808a0,0,0,0,1,0,0V773a300,300,0,0,1-300,300H0a0,0,0,0,1,0,0V0A0,0,0,0,1,0,0Z" transform="translate(0 993)" fill="#484847"/>
    <g id="Mask_Group_104" data-name="Mask Group 104" clip-path="url(#clip-path)">
      <g id="Group_1804" data-name="Group 1804" transform="matrix(-0.574, 0.819, -0.819, -0.574, 2317.253, 1131.362)">
        <g id="Group_1711" data-name="Group 1711" transform="translate(1590.864 0.001)">
          <g id="Group_1710" data-name="Group 1710" transform="translate(0 0)" clip-path="url(#clip-path-2)">
            <rect id="Rectangle_1536" data-name="Rectangle 1536" width="158.621" height="311.617" transform="translate(-4.625 9.579) rotate(-64.226)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1713" data-name="Group 1713" transform="translate(1382.445 0.001)">
          <g id="Group_1712" data-name="Group 1712" transform="translate(0 0)" clip-path="url(#clip-path-3)">
            <rect id="Rectangle_1537" data-name="Rectangle 1537" width="270.408" height="509.52" transform="translate(-13.827 28.636) rotate(-64.226)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1715" data-name="Group 1715" transform="translate(1235.907 0.001)">
          <g id="Group_1714" data-name="Group 1714" transform="translate(0 0)" clip-path="url(#clip-path-4)">
            <rect id="Rectangle_1538" data-name="Rectangle 1538" width="353.722" height="650.943" transform="translate(-22.348 46.284) rotate(-64.226)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1717" data-name="Group 1717" transform="translate(1113.1 0.001)">
          <g id="Group_1716" data-name="Group 1716" transform="translate(0 0)" clip-path="url(#clip-path-5)">
            <rect id="Rectangle_1539" data-name="Rectangle 1539" width="426.652" height="770.964" transform="translate(-30.841 63.872) rotate(-64.226)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1719" data-name="Group 1719" transform="translate(1003.711 0.001)">
          <g id="Group_1718" data-name="Group 1718" transform="translate(0 0)" clip-path="url(#clip-path-6)">
            <rect id="Rectangle_1540" data-name="Rectangle 1540" width="493.682" height="878.869" transform="translate(-39.305 81.402) rotate(-64.226)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1721" data-name="Group 1721" transform="translate(903.028 0.001)">
          <g id="Group_1720" data-name="Group 1720" transform="translate(0 0)" clip-path="url(#clip-path-7)">
            <rect id="Rectangle_1541" data-name="Rectangle 1541" width="556.817" height="978.883" transform="matrix(0.435, -0.901, 0.901, 0.435, -47.722, 98.833)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1723" data-name="Group 1723" transform="translate(808.415 0.001)">
          <g id="Group_1722" data-name="Group 1722" transform="translate(0 0)" clip-path="url(#clip-path-8)">
            <rect id="Rectangle_1542" data-name="Rectangle 1542" width="617.223" height="1073.387" transform="translate(-56.099 116.182) rotate(-64.226)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1725" data-name="Group 1725" transform="translate(718.191 0.001)">
          <g id="Group_1724" data-name="Group 1724" transform="translate(0 0)" clip-path="url(#clip-path-9)">
            <rect id="Rectangle_1543" data-name="Rectangle 1543" width="675.594" height="1163.877" transform="matrix(0.435, -0.901, 0.901, 0.435, -64.421, 133.419)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1727" data-name="Group 1727" transform="translate(631.497 0.001)">
          <g id="Group_1726" data-name="Group 1726" transform="translate(0 0)" clip-path="url(#clip-path-10)">
            <rect id="Rectangle_1544" data-name="Rectangle 1544" width="732.8" height="1251.366" transform="translate(-72.905 150.988) rotate(-64.226)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1729" data-name="Group 1729" transform="translate(547.33 0.001)">
          <g id="Group_1728" data-name="Group 1728" transform="translate(0 0)" clip-path="url(#clip-path-11)">
            <rect id="Rectangle_1545" data-name="Rectangle 1545" width="788.913" height="1336.584" transform="translate(-81.39 168.562) rotate(-64.226)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1731" data-name="Group 1731" transform="translate(465.481 0.001)">
          <g id="Group_1730" data-name="Group 1730" transform="translate(0 0)" clip-path="url(#clip-path-12)">
            <rect id="Rectangle_1546" data-name="Rectangle 1546" width="843.804" height="1419.61" transform="matrix(0.435, -0.901, 0.901, 0.435, -89.783, 185.943)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1733" data-name="Group 1733" transform="translate(385.395 0.001)">
          <g id="Group_1732" data-name="Group 1732" transform="translate(0 0)" clip-path="url(#clip-path-13)">
            <rect id="Rectangle_1547" data-name="Rectangle 1547" width="897.718" height="1500.948" transform="matrix(0.435, -0.901, 0.901, 0.435, -98.084, 203.136)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1735" data-name="Group 1735" transform="translate(306.78 0.001)">
          <g id="Group_1734" data-name="Group 1734" transform="translate(0 0)" clip-path="url(#clip-path-14)">
            <rect id="Rectangle_1548" data-name="Rectangle 1548" width="950.763" height="1580.85" transform="translate(-106.286 220.121) rotate(-64.226)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1737" data-name="Group 1737" transform="translate(229.452 0.001)">
          <g id="Group_1736" data-name="Group 1736" transform="translate(0 0)" clip-path="url(#clip-path-15)">
            <rect id="Rectangle_1549" data-name="Rectangle 1549" width="1003.031" height="1659.487" transform="matrix(0.435, -0.901, 0.901, 0.435, -114.393, 236.911)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1739" data-name="Group 1739" transform="translate(153.358 0.001)">
          <g id="Group_1738" data-name="Group 1738" transform="translate(0 0)" clip-path="url(#clip-path-16)">
            <rect id="Rectangle_1550" data-name="Rectangle 1550" width="1054.471" height="1736.874" transform="matrix(0.435, -0.901, 0.901, 0.435, -122.373, 253.439)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1741" data-name="Group 1741" transform="translate(78.375 0.001)">
          <g id="Group_1740" data-name="Group 1740" transform="translate(0 0)" clip-path="url(#clip-path-17)">
            <rect id="Rectangle_1551" data-name="Rectangle 1551" width="1107.146" height="1814.089" transform="matrix(0.435, -0.901, 0.901, 0.435, -131.1, 271.513)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1743" data-name="Group 1743" transform="translate(4.466 0.001)">
          <g id="Group_1742" data-name="Group 1742" transform="translate(0 0)" clip-path="url(#clip-path-18)">
            <rect id="Rectangle_1552" data-name="Rectangle 1552" width="1160.035" height="1890.665" transform="translate(-140.123 290.2) rotate(-64.226)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1745" data-name="Group 1745" transform="translate(0 26.182)">
          <g id="Group_1744" data-name="Group 1744" clip-path="url(#clip-path-19)">
            <rect id="Rectangle_1553" data-name="Rectangle 1553" width="1158.524" height="1893.019" transform="matrix(0.435, -0.901, 0.901, 0.435, -138.622, 287.091)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1747" data-name="Group 1747" transform="translate(0 67.689)">
          <g id="Group_1746" data-name="Group 1746" clip-path="url(#clip-path-20)">
            <rect id="Rectangle_1554" data-name="Rectangle 1554" width="1140.683" height="1884.405" transform="matrix(0.435, -0.901, 0.901, 0.435, -130.865, 271.025)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1749" data-name="Group 1749" transform="translate(0 106.854)">
          <g id="Group_1748" data-name="Group 1748" clip-path="url(#clip-path-21)">
            <rect id="Rectangle_1555" data-name="Rectangle 1555" width="1124.386" height="1876.537" transform="translate(-123.779 256.35) rotate(-64.226)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1751" data-name="Group 1751" transform="translate(0 144.079)">
          <g id="Group_1750" data-name="Group 1750" clip-path="url(#clip-path-22)">
            <rect id="Rectangle_1556" data-name="Rectangle 1556" width="1109.284" height="1869.244" transform="translate(-117.212 242.749) rotate(-64.226)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1753" data-name="Group 1753" transform="translate(0 179.606)">
          <g id="Group_1752" data-name="Group 1752" clip-path="url(#clip-path-23)">
            <rect id="Rectangle_1557" data-name="Rectangle 1557" width="1095.209" height="1862.448" transform="translate(-111.092 230.075) rotate(-64.226)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1755" data-name="Group 1755" transform="translate(0 213.66)">
          <g id="Group_1754" data-name="Group 1754" clip-path="url(#clip-path-24)">
            <rect id="Rectangle_1558" data-name="Rectangle 1558" width="1081.963" height="1856.052" transform="translate(-105.332 218.147) rotate(-64.226)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1757" data-name="Group 1757" transform="translate(0 246.434)">
          <g id="Group_1756" data-name="Group 1756" clip-path="url(#clip-path-25)">
            <rect id="Rectangle_1559" data-name="Rectangle 1559" width="1069.443" height="1850.007" transform="matrix(0.435, -0.901, 0.901, 0.435, -99.888, 206.872)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1759" data-name="Group 1759" transform="translate(0 278.043)">
          <g id="Group_1758" data-name="Group 1758" clip-path="url(#clip-path-26)">
            <rect id="Rectangle_1560" data-name="Rectangle 1560" width="1058.638" height="1844.789" transform="translate(-95.19 197.142) rotate(-64.226)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1761" data-name="Group 1761" transform="translate(0 308.621)">
          <g id="Group_1760" data-name="Group 1760" clip-path="url(#clip-path-27)">
            <rect id="Rectangle_1561" data-name="Rectangle 1561" width="1049.24" height="1840.252" transform="translate(-91.104 188.679) rotate(-64.226)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1763" data-name="Group 1763" transform="translate(0 338.257)">
          <g id="Group_1762" data-name="Group 1762" clip-path="url(#clip-path-28)">
            <rect id="Rectangle_1562" data-name="Rectangle 1562" width="1040.21" height="1835.891" transform="matrix(0.435, -0.901, 0.901, 0.435, -87.177, 180.547)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1765" data-name="Group 1765" transform="translate(0 367.019)">
          <g id="Group_1764" data-name="Group 1764" clip-path="url(#clip-path-29)">
            <rect id="Rectangle_1563" data-name="Rectangle 1563" width="1031.162" height="1831.523" transform="translate(-83.243 172.4) rotate(-64.226)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1767" data-name="Group 1767" transform="translate(0 394.996)">
          <g id="Group_1766" data-name="Group 1766" clip-path="url(#clip-path-30)">
            <rect id="Rectangle_1564" data-name="Rectangle 1564" width="1022.48" height="1827.33" transform="matrix(0.435, -0.901, 0.901, 0.435, -79.468, 164.581)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1769" data-name="Group 1769" transform="translate(0 422.224)">
          <g id="Group_1768" data-name="Group 1768" clip-path="url(#clip-path-31)">
            <rect id="Rectangle_1565" data-name="Rectangle 1565" width="1014.175" height="1823.32" transform="translate(-75.857 157.102) rotate(-64.226)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1771" data-name="Group 1771" transform="translate(0 448.772)">
          <g id="Group_1770" data-name="Group 1770" clip-path="url(#clip-path-32)">
            <rect id="Rectangle_1566" data-name="Rectangle 1566" width="1006.198" height="1819.469" transform="matrix(0.435, -0.901, 0.901, 0.435, -72.388, 149.919)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1773" data-name="Group 1773" transform="translate(0 474.678)">
          <g id="Group_1772" data-name="Group 1772" clip-path="url(#clip-path-33)">
            <rect id="Rectangle_1567" data-name="Rectangle 1567" width="998.478" height="1815.741" transform="translate(-69.032 142.967) rotate(-64.226)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1775" data-name="Group 1775" transform="translate(0 499.978)">
          <g id="Group_1774" data-name="Group 1774" clip-path="url(#clip-path-34)">
            <rect id="Rectangle_1568" data-name="Rectangle 1568" width="991.129" height="1812.193" transform="matrix(0.435, -0.901, 0.901, 0.435, -65.836, 136.349)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1777" data-name="Group 1777" transform="translate(0 524.718)">
          <g id="Group_1776" data-name="Group 1776" clip-path="url(#clip-path-35)">
            <rect id="Rectangle_1569" data-name="Rectangle 1569" width="983.976" height="1808.739" transform="matrix(0.435, -0.901, 0.901, 0.435, -62.726, 129.907)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1779" data-name="Group 1779" transform="translate(0 548.92)">
          <g id="Group_1778" data-name="Group 1778" clip-path="url(#clip-path-36)">
            <rect id="Rectangle_1570" data-name="Rectangle 1570" width="977.249" height="1805.491" transform="matrix(0.435, -0.901, 0.901, 0.435, -59.801, 123.85)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1781" data-name="Group 1781" transform="translate(0 572.614)">
          <g id="Group_1780" data-name="Group 1780" clip-path="url(#clip-path-37)">
            <rect id="Rectangle_1571" data-name="Rectangle 1571" width="973.241" height="1803.556" transform="translate(-58.058 120.241) rotate(-64.226)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1783" data-name="Group 1783" transform="translate(0 595.837)">
          <g id="Group_1782" data-name="Group 1782" clip-path="url(#clip-path-38)">
            <rect id="Rectangle_1572" data-name="Rectangle 1572" width="969.21" height="1801.609" transform="matrix(0.435, -0.901, 0.901, 0.435, -56.306, 116.61)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1785" data-name="Group 1785" transform="translate(0 618.595)">
          <g id="Group_1784" data-name="Group 1784" clip-path="url(#clip-path-39)">
            <rect id="Rectangle_1573" data-name="Rectangle 1573" width="965.255" height="1799.699" transform="matrix(0.435, -0.901, 0.901, 0.435, -54.586, 113.049)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1787" data-name="Group 1787" transform="translate(0 640.907)">
          <g id="Group_1786" data-name="Group 1786" clip-path="url(#clip-path-40)">
            <rect id="Rectangle_1574" data-name="Rectangle 1574" width="958.704" height="1796.536" transform="matrix(0.435, -0.901, 0.901, 0.435, -51.737, 107.15)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1789" data-name="Group 1789" transform="translate(0 662.814)">
          <g id="Group_1788" data-name="Group 1788" clip-path="url(#clip-path-41)">
            <rect id="Rectangle_1575" data-name="Rectangle 1575" width="958.341" height="1796.361" transform="matrix(0.435, -0.901, 0.901, 0.435, -51.58, 106.823)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1791" data-name="Group 1791" transform="translate(0 684.319)">
          <g id="Group_1790" data-name="Group 1790" clip-path="url(#clip-path-42)">
            <rect id="Rectangle_1576" data-name="Rectangle 1576" width="953.853" height="1794.194" transform="translate(-49.628 102.781) rotate(-64.226)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1793" data-name="Group 1793" transform="translate(0 705.416)">
          <g id="Group_1792" data-name="Group 1792" clip-path="url(#clip-path-43)">
            <rect id="Rectangle_1577" data-name="Rectangle 1577" width="433.346" height="752.059" transform="translate(-39.813 82.453) rotate(-64.226)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1795" data-name="Group 1795" transform="translate(1581.022 775.463)">
          <g id="Group_1794" data-name="Group 1794" clip-path="url(#clip-path-44)">
            <rect id="Rectangle_1578" data-name="Rectangle 1578" width="180.747" height="329.097" transform="translate(-12.385 25.65) rotate(-64.226)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1797" data-name="Group 1797" transform="translate(0 726.197)">
          <g id="Group_1796" data-name="Group 1796" clip-path="url(#clip-path-45)">
            <rect id="Rectangle_1579" data-name="Rectangle 1579" width="316.027" height="538.806" transform="matrix(0.435, -0.901, 0.901, 0.435, -31.676, 65.601)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1799" data-name="Group 1799" transform="translate(0 746.584)">
          <g id="Group_1798" data-name="Group 1798" clip-path="url(#clip-path-46)">
            <rect id="Rectangle_1580" data-name="Rectangle 1580" width="223.988" height="377.349" transform="translate(-23.693 49.069) rotate(-64.226)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1801" data-name="Group 1801" transform="translate(0 766.663)">
          <g id="Group_1800" data-name="Group 1800" clip-path="url(#clip-path-47)">
            <rect id="Rectangle_1581" data-name="Rectangle 1581" width="144.105" height="240.623" transform="translate(-15.831 32.786) rotate(-64.226)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
        <g id="Group_1803" data-name="Group 1803" transform="translate(0 786.36)">
          <g id="Group_1802" data-name="Group 1802" clip-path="url(#clip-path-48)">
            <rect id="Rectangle_1582" data-name="Rectangle 1582" width="71.504" height="118.437" transform="matrix(0.435, -0.901, 0.901, 0.435, -8.118, 16.812)" fill="rgba(255,255,255,0.03)"/>
          </g>
        </g>
      </g>
    </g>
  </g>
  <g id="Ellipse_100" data-name="Ellipse 100" transform="translate(311.399 267.811)" fill="none" stroke="#707070" stroke-width="2">
    <ellipse cx="227.531" cy="227.531" rx="227.531" ry="227.531" stroke="none"/>
    <ellipse cx="227.531" cy="227.531" rx="226.531" ry="226.531" fill="none"/>
  </g>
  <g id="Group_1682" data-name="Group 1682" transform="translate(311.399 269.997)">
    <g id="Group_1675" data-name="Group 1675" transform="translate(0 0)">
      <path id="Path_886" data-name="Path 886" d="M519.856,343.2a228.566,228.566,0,0,0,0-164.84" transform="translate(-79.57 -35.598)" fill="none" stroke="#5a5a5a" stroke-linecap="round" stroke-miterlimit="10" stroke-width="29"/>
      <path id="Path_887" data-name="Path 887" d="M458.707,96.929A227.6,227.6,0,0,0,316,14.5" transform="translate(-53.321 -14.499)" fill="none" stroke="#5a5a5a" stroke-linecap="round" stroke-miterlimit="10" stroke-width="29"/>
      <path id="Path_888" data-name="Path 888" d="M214.912,14.5a227.6,227.6,0,0,0-142.7,82.43" transform="translate(-21.93 -14.499)" fill="none" stroke="#5a5a5a" stroke-linecap="round" stroke-miterlimit="10" stroke-width="29"/>
      <path id="Path_889" data-name="Path 889" d="M72.208,436.809a227.6,227.6,0,0,0,142.7,82.43" transform="translate(-21.93 -68.875)" fill="none" stroke="#5a5a5a" stroke-linecap="round" stroke-miterlimit="10" stroke-width="29"/>
      <path id="Path_890" data-name="Path 890" d="M316,519.239a227.6,227.6,0,0,0,142.7-82.43" transform="translate(-53.321 -68.875)" fill="none" stroke="#5a5a5a" stroke-linecap="round" stroke-miterlimit="10" stroke-width="29"/>
      <path id="Path_891" data-name="Path 891" d="M29.877,178.36a228.566,228.566,0,0,0,0,164.84" transform="translate(-14.5 -35.598)" fill="none" stroke="#5a5a5a" stroke-linecap="round" stroke-miterlimit="10" stroke-width="29"/>
    </g>
  </g>
  <ellipse id="Ellipse_99" data-name="Ellipse 99" cx="154.145" cy="154.145" rx="154.145" ry="154.145" transform="translate(384.645 341.057)" fill="#fff" opacity="0.155"/>
  <g id="Group_1676" data-name="Group 1676" transform="translate(332.544 213.195)" opacity="0.998">
    <rect id="Rectangle_1485" data-name="Rectangle 1485" width="3.257" height="3.257" transform="translate(37.467 47.238)" fill="#e2e2e2"/>
    <rect id="Rectangle_1486" data-name="Rectangle 1486" width="3.257" height="3.257" transform="translate(8.147 47.238)" fill="#e2e2e2"/>
    <rect id="Rectangle_1487" data-name="Rectangle 1487" width="3.257" height="3.257" transform="translate(27.693 47.238)" fill="#e2e2e2"/>
    <rect id="Rectangle_1488" data-name="Rectangle 1488" width="3.257" height="3.257" transform="translate(17.922 47.238)" fill="#e2e2e2"/>
    <rect id="Rectangle_1489" data-name="Rectangle 1489" width="3.257" height="3.257" transform="translate(66.786 47.238)" fill="#e2e2e2"/>
    <rect id="Rectangle_1490" data-name="Rectangle 1490" width="3.257" height="3.257" transform="translate(47.24 47.238)" fill="#e2e2e2"/>
    <rect id="Rectangle_1491" data-name="Rectangle 1491" width="3.257" height="3.257" transform="translate(57.014 47.238)" fill="#e2e2e2"/>
    <path id="Path_850" data-name="Path 850" d="M821.964,36.776l-2.975-1.327,7.425-16.607c1.852-3.843,3.449-7.163,7.852-7.163h1.629v3.259h-1.629c-2.261,0-3.139,1.629-4.917,5.318l-.133.276Z" transform="translate(-817.973 9.495)" fill="#e2e2e2"/>
    <path id="Path_851" data-name="Path 851" d="M888.55,37.245h-61.9a8.144,8.144,0,1,1,0-16.288h61.9a8.144,8.144,0,1,1,0,16.288m-61.9-13.031a4.887,4.887,0,0,0,0,9.774h61.9a4.887,4.887,0,0,0,0-9.774Z" transform="translate(-818.507 19.765)" fill="#e2e2e2"/>
    <path id="Path_852" data-name="Path 852" d="M861.044,36.776l-7.385-16.52c-1.778-3.689-2.657-5.318-4.918-5.318h-1.629V11.679h1.629c4.4,0,6,3.32,7.85,7.163l.133.274,7.294,16.333Z" transform="translate(-786.843 9.495)" fill="#e2e2e2"/>
    <path id="Path_853" data-name="Path 853" d="M865.251,37.465H829.415a1.628,1.628,0,0,1-1.629-1.629V11.4a1.656,1.656,0,0,1,.116-.6l3.257-8.143a1.631,1.631,0,0,1,1.513-1.026h29.32A1.627,1.627,0,0,1,863.5,2.655l3.259,8.143a1.656,1.656,0,0,1,.116.6V35.836a1.628,1.628,0,0,1-1.629,1.629m-34.208-3.257h32.579V11.717l-2.733-6.828H833.774l-2.731,6.828Z" transform="translate(-808.236 -1.629)" fill="#e2e2e2"/>
    <rect id="Rectangle_1492" data-name="Rectangle 1492" width="35.836" height="3.257" transform="translate(21.177 8.145)" fill="#e2e2e2"/>
  </g>
  <g id="Group_1677" data-name="Group 1677" transform="translate(202.504 440.51)">
    <path id="Path_854" data-name="Path 854" d="M22.221,29.346H5.541V12.666h16.68ZM8.878,26.011H18.886V16H8.878Z" transform="translate(6.133 14.02)" fill="#e2e2e2"/>
    <path id="Path_855" data-name="Path 855" d="M33.305,34.1H16.625V17.416h16.68ZM19.96,30.761H29.968V20.753H19.96Z" transform="translate(18.402 19.278)" fill="#e2e2e2"/>
    <path id="Path_856" data-name="Path 856" d="M42.805,34.1H26.125V17.416h16.68ZM29.46,30.761H39.468V20.753H29.46Z" transform="translate(28.918 19.278)" fill="#e2e2e2"/>
    <rect id="Rectangle_1493" data-name="Rectangle 1493" width="3.335" height="3.335" transform="translate(68.388 56.711)" fill="#e2e2e2"/>
    <rect id="Rectangle_1494" data-name="Rectangle 1494" width="3.335" height="3.335" transform="translate(8.341 56.711)" fill="#e2e2e2"/>
    <rect id="Rectangle_1495" data-name="Rectangle 1495" width="3.335" height="3.335" transform="translate(18.349 56.711)" fill="#e2e2e2"/>
    <rect id="Rectangle_1496" data-name="Rectangle 1496" width="3.335" height="3.335" transform="translate(28.357 56.711)" fill="#e2e2e2"/>
    <rect id="Rectangle_1497" data-name="Rectangle 1497" width="3.335" height="3.335" transform="translate(38.364 56.711)" fill="#e2e2e2"/>
    <rect id="Rectangle_1498" data-name="Rectangle 1498" width="3.335" height="3.335" transform="translate(48.372 56.711)" fill="#e2e2e2"/>
    <rect id="Rectangle_1499" data-name="Rectangle 1499" width="3.335" height="3.335" transform="translate(58.38 56.711)" fill="#e2e2e2"/>
    <path id="Path_857" data-name="Path 857" d="M71.721,40.428H8.339a8.339,8.339,0,0,1,0-16.678H71.721a8.339,8.339,0,1,1,0,16.678M8.339,27.085a5,5,0,0,0,0,10.008H71.721a5,5,0,1,0,0-10.008Z" transform="translate(0 26.289)" fill="#e2e2e2"/>
    <path id="Path_858" data-name="Path 858" d="M24.8,30.488,23.313,27.5l5.75-2.876V15.015L17.386,9.177,5.712,15.015v9.614l5.75,2.876L9.968,30.488l-7.593-3.8V12.953l15.012-7.5,15.012,7.5V26.692Z" transform="translate(2.629 6.03)" fill="#e2e2e2"/>
    <rect id="Rectangle_1500" data-name="Rectangle 1500" width="3.335" height="13.343" transform="translate(18.349)" fill="#e2e2e2"/>
  </g>
  <g id="Group_1678" data-name="Group 1678" transform="translate(684.736 213.195)" opacity="0.998">
    <rect id="Rectangle_1480" data-name="Rectangle 1480" width="20.816" height="3.103" transform="matrix(0.447, -0.894, 0.894, 0.447, 3.267, 69.122)" fill="#e2e2e2"/>
    <rect id="Rectangle_1481" data-name="Rectangle 1481" width="15.063" height="3.105" transform="translate(28.159 69.006) rotate(-58.456)" fill="#e2e2e2"/>
    <path id="Path_843" data-name="Path 843" d="M24.654,783.272A13.963,13.963,0,1,1,38.619,769.31a13.979,13.979,0,0,1-13.964,13.962m0-24.823A10.86,10.86,0,1,0,35.515,769.31a10.875,10.875,0,0,0-10.861-10.861" transform="translate(1.724 -718.108)" fill="#e2e2e2"/>
    <path id="Path_844" data-name="Path 844" d="M20.579,771.439a6.206,6.206,0,1,1,6.207-6.2,6.213,6.213,0,0,1-6.207,6.2m0-9.308a3.1,3.1,0,1,0,3.1,3.1,3.108,3.108,0,0,0-3.1-3.1" transform="translate(5.799 -714.032)" fill="#e2e2e2"/>
    <path id="Path_845" data-name="Path 845" d="M34.727,757.763a9.309,9.309,0,1,1,9.308-9.308,9.319,9.319,0,0,1-9.308,9.308m0-15.515a6.206,6.206,0,1,0,6.207,6.207,6.212,6.212,0,0,0-6.207-6.207" transform="translate(18.027 -736.04)" fill="#e2e2e2"/>
    <path id="Path_846" data-name="Path 846" d="M17.212,762.5a12.413,12.413,0,1,1,12.412-12.412A12.427,12.427,0,0,1,17.212,762.5m0-21.722a9.31,9.31,0,1,0,9.308,9.31,9.323,9.323,0,0,0-9.308-9.31" transform="translate(-4.798 -737.671)" fill="#e2e2e2"/>
    <path id="Path_847" data-name="Path 847" d="M39.933,744.534h-8.4l-1.551-1.551H15.109v-3.1H31.267l1.551,1.551h7.115Z" transform="translate(6.615 -735.225)" fill="#e2e2e2"/>
    <path id="Path_848" data-name="Path 848" d="M30.452,748.953H15.845v-3.1H29.167l1.553-1.551h8.4v3.1H32Z" transform="translate(7.429 -730.335)" fill="#e2e2e2"/>
    <rect id="Rectangle_1482" data-name="Rectangle 1482" width="3.103" height="33.632" transform="translate(0.863 16.972) rotate(-21.151)" fill="#e2e2e2"/>
    <rect id="Rectangle_1483" data-name="Rectangle 1483" width="3.104" height="29.71" transform="translate(20.382 19.4) rotate(-30.236)" fill="#e2e2e2"/>
    <path id="Path_849" data-name="Path 849" d="M36.106,771.332H31.719l-4.014-4.012v-3.1l-1.551-1.551v-10.6l1.551-1.551v-4.012h3.1v5.3l-1.553,1.551v8.025l1.553,1.551v3.1l2.2,2.193h1.816l2.193-2.193v-3.1l1.553-1.551v-8.025L37.014,751.8v-5.3h3.1v4.012l1.553,1.551v10.6l-1.553,1.551v3.1Z" transform="translate(18.842 -727.889)" fill="#e2e2e2"/>
    <rect id="Rectangle_1484" data-name="Rectangle 1484" width="3.103" height="10.861" transform="translate(51.202 41.891)" fill="#e2e2e2"/>
  </g>
  <g id="Group_1679" data-name="Group 1679" transform="translate(813.668 440.51)">
    <path id="Path_837" data-name="Path 837" d="M848.713,799.117H844.3a16.428,16.428,0,0,1-16.409-16.408V765.068A16.428,16.428,0,0,1,844.3,748.659h4.412a16.428,16.428,0,0,1,16.408,16.409v17.641a16.428,16.428,0,0,1-16.408,16.408M844.3,751.6a13.478,13.478,0,0,0-13.463,13.463v17.641A13.48,13.48,0,0,0,844.3,796.172h4.412a13.478,13.478,0,0,0,13.461-13.463V765.068A13.476,13.476,0,0,0,848.713,751.6Z" transform="translate(-827.893 -722.531)" fill="#e2e2e2"/>
    <path id="Path_838" data-name="Path 838" d="M837.5,765.619a1.473,1.473,0,0,1-1.473-1.473V750.132a1.473,1.473,0,0,1,2.945,0v14.015a1.473,1.473,0,0,1-1.473,1.473" transform="translate(-818.887 -722.531)" fill="#e2e2e2"/>
    <path id="Path_839" data-name="Path 839" d="M839.617,766.307a5.5,5.5,0,1,1,5.5-5.5,5.5,5.5,0,0,1-5.5,5.5m0-8.05a2.553,2.553,0,1,0,2.554,2.554,2.556,2.556,0,0,0-2.554-2.554" transform="translate(-821.003 -715.168)" fill="#e2e2e2"/>
    <path id="Path_840" data-name="Path 840" d="M848.48,748.97a1.461,1.461,0,0,1-1.041-.432l-.274-.274a8.515,8.515,0,0,0-12.039,0,1.472,1.472,0,0,1-2.082-2.082,11.457,11.457,0,0,1,16.2,0l.272.274a1.473,1.473,0,0,1-1.041,2.514" transform="translate(-822.668 -728.988)" fill="#e2e2e2"/>
    <path id="Path_841" data-name="Path 841" d="M855.29,747.69a1.461,1.461,0,0,1-1.041-.432l-.442-.443a14.727,14.727,0,0,0-20.8,0,1.473,1.473,0,0,1-2.082-2.084,17.669,17.669,0,0,1,24.962,0l.443.445a1.473,1.473,0,0,1-1.041,2.514" transform="translate(-825.014 -732.592)" fill="#e2e2e2"/>
    <path id="Path_842" data-name="Path 842" d="M862.8,746.571a1.461,1.461,0,0,1-1.041-.432l-.63-.63a21.536,21.536,0,0,0-30.453,0,1.472,1.472,0,0,1-2.082-2.082,24.476,24.476,0,0,1,34.616,0l.63.63a1.473,1.473,0,0,1-1.041,2.513" transform="translate(-827.599 -736.258)" fill="#e2e2e2"/>
  </g>
  <g id="Group_1681" data-name="Group 1681" transform="translate(661.231 720.569)">
    <g id="Group_1680" data-name="Group 1680" transform="translate(0 0)" clip-path="url(#clip-path-49)">
      <path id="Path_892" data-name="Path 892" d="M38.184,32.015a1.559,1.559,0,0,1-1.11-.461L22.422,16.9a1.571,1.571,0,0,1,0-2.223L35.355,1.746a3.039,3.039,0,0,1,4.2,0L52.23,14.419a2.981,2.981,0,0,1,0,4.2L39.3,31.554a1.56,1.56,0,0,1-1.113.461M25.755,15.792,38.184,28.221l11.7-11.7L37.456,4.091Z" transform="translate(28.624 1.176)" fill="#e2e2e2"/>
      <path id="Path_893" data-name="Path 893" d="M28.618,10.157a1.572,1.572,0,0,1-1.11-2.683l2.147-2.147A1.572,1.572,0,1,1,31.878,7.55L29.731,9.7a1.574,1.574,0,0,1-1.113.461" transform="translate(35.252 6.341)" fill="#e2e2e2"/>
      <path id="Path_894" data-name="Path 894" d="M30.779,12.32a1.572,1.572,0,0,1-1.11-2.683l2.147-2.149A1.572,1.572,0,0,1,34.039,9.71l-2.147,2.149a1.57,1.57,0,0,1-1.113.461" transform="translate(38.069 9.159)" fill="#e2e2e2"/>
      <path id="Path_895" data-name="Path 895" d="M24.972,50.842a1.558,1.558,0,0,1-1.11-.461L11.675,38.2a1.574,1.574,0,0,1-.3-1.81l8.1-16.28a1.574,1.574,0,0,1,.295-.41l13.8-13.8a1.618,1.618,0,0,1,2.223,0L56.162,26.264a1.574,1.574,0,0,1,0,2.223l-13.8,13.8a1.57,1.57,0,0,1-.412.295l-16.278,8.1a1.569,1.569,0,0,1-.7.164M14.7,36.773,25.285,47.361l15.037-7.479L52.829,27.375,34.681,9.226,22.173,21.736Z" transform="translate(14.616 7.104)" fill="#e2e2e2"/>
      <path id="Path_896" data-name="Path 896" d="M18.868,36.772a1.568,1.568,0,0,1-1.11-.461L9.871,28.424a1.57,1.57,0,0,1,0-2.223l6.3-6.307a1.574,1.574,0,0,1,2.223,0l7.887,7.889a1.567,1.567,0,0,1,0,2.22l-6.3,6.307a1.57,1.57,0,0,1-1.113.461M13.2,27.312l5.664,5.664,4.084-4.082-5.664-5.666Z" transform="translate(12.265 25.331)" fill="#e2e2e2"/>
      <path id="Path_897" data-name="Path 897" d="M16.967,65.594A8.27,8.27,0,0,1,11.1,63.171L2.423,54.489a8.3,8.3,0,0,1,0-11.724L33.261,11.927a5.141,5.141,0,0,0,0-7.276,5.267,5.267,0,0,0-7.279,0L14.6,16.032a1.571,1.571,0,0,1-2.22-2.223L23.762,2.428a8.289,8.289,0,0,1,14.15,5.86,8.232,8.232,0,0,1-2.428,5.862L4.646,44.988a5.154,5.154,0,0,0,0,7.279l8.681,8.681a5.158,5.158,0,0,0,7.279,0l5.473-5.473A1.572,1.572,0,0,1,28.3,57.7l-5.473,5.473a8.27,8.27,0,0,1-5.862,2.423" transform="translate(0 -0.001)" fill="#e2e2e2"/>
    </g>
  </g>
  <g id="Group_1684" data-name="Group 1684" transform="translate(358.695 720.197)">
    <g id="Group_1683" data-name="Group 1683" transform="translate(0 0)" clip-path="url(#clip-path-50)">
      <path id="Path_898" data-name="Path 898" d="M51.825,22.32a23.38,23.38,0,0,0-1.732-4.185l4.691-4.691L45.011,3.676,40.322,8.365a23.3,23.3,0,0,0-4.185-1.73V0H22.32V6.635a23.3,23.3,0,0,0-4.184,1.73L13.445,3.676,3.675,13.445l4.691,4.691A23.381,23.381,0,0,0,6.634,22.32H0V34.544H7.971V31.887H2.656V24.977H8.67l.257-1a20.781,20.781,0,0,1,2.234-5.4l.523-.888L7.433,13.445l6.012-6.012L17.7,11.684l.888-.523a20.766,20.766,0,0,1,5.4-2.233l1-.259V2.656h8.5V8.67l1,.259a20.766,20.766,0,0,1,5.4,2.233l.886.523,4.251-4.251,6.013,6.012L46.774,17.7l.523.888a20.781,20.781,0,0,1,2.234,5.4l.257,1H55.8v6.911H50.487v2.656h7.971V22.32Z" transform="translate(0 0.001)" fill="#e2e2e2"/>
      <path id="Path_899" data-name="Path 899" d="M27.416,5.79A18.635,18.635,0,0,0,8.8,24.4v4.889l-6.643,9.3v8.376H4.8V39.438l6.643-9.3V24.4a15.975,15.975,0,0,1,31.95-.017v5.757l6.643,9.3v7.525H52.67V38.587l-6.643-9.3V24.383A18.626,18.626,0,0,0,27.416,5.79" transform="translate(1.804 4.833)" fill="#e2e2e2"/>
      <rect id="Rectangle_1519" data-name="Rectangle 1519" width="2.741" height="4.007" transform="translate(39.816 38.51)" fill="#e2e2e2"/>
      <path id="Path_900" data-name="Path 900" d="M35.287,21.967a13.286,13.286,0,1,0-26.573.013v6.643H11.35V21.98a10.6,10.6,0,1,1,21.2-.013v6.656h2.741Z" transform="translate(7.272 7.25)" fill="#e2e2e2"/>
      <rect id="Rectangle_1520" data-name="Rectangle 1520" width="2.636" height="4.007" transform="translate(15.986 38.51)" fill="#e2e2e2"/>
      <path id="Path_901" data-name="Path 901" d="M5.315,28.97a5.314,5.314,0,1,0,5.315,5.315A5.32,5.32,0,0,0,5.315,28.97m0,7.971a2.657,2.657,0,1,1,2.658-2.656,2.659,2.659,0,0,1-2.658,2.656" transform="translate(0 24.176)" fill="#e2e2e2"/>
      <path id="Path_902" data-name="Path 902" d="M11.833,23.9a5.314,5.314,0,1,0,5.315,5.315A5.32,5.32,0,0,0,11.833,23.9m0,7.971a2.657,2.657,0,1,1,2.656-2.656,2.659,2.659,0,0,1-2.656,2.656" transform="translate(5.439 19.945)" fill="#e2e2e2"/>
      <path id="Path_903" data-name="Path 903" d="M24.869,23.9a5.314,5.314,0,1,0,5.315,5.315A5.32,5.32,0,0,0,24.869,23.9m0,7.971a2.657,2.657,0,1,1,2.658-2.656,2.659,2.659,0,0,1-2.658,2.656" transform="translate(16.318 19.945)" fill="#e2e2e2"/>
      <path id="Path_904" data-name="Path 904" d="M31.388,28.97A5.314,5.314,0,1,0,36.7,34.285a5.32,5.32,0,0,0-5.315-5.315m0,7.971a2.657,2.657,0,1,1,2.656-2.656,2.659,2.659,0,0,1-2.656,2.656" transform="translate(21.758 24.176)" fill="#e2e2e2"/>
      <path id="Path_905" data-name="Path 905" d="M18.351,28.97a5.314,5.314,0,1,0,5.315,5.315,5.32,5.32,0,0,0-5.315-5.315m0,7.971a2.657,2.657,0,1,1,2.656-2.656,2.661,2.661,0,0,1-2.656,2.656" transform="translate(10.879 24.176)" fill="#e2e2e2"/>
      <path id="Path_906" data-name="Path 906" d="M19.559,11.588a7.971,7.971,0,0,0-1.328,15.83V42.145h2.656V27.418a7.971,7.971,0,0,0-1.328-15.83m0,13.285a5.315,5.315,0,1,1,5.315-5.315,5.32,5.32,0,0,1-5.315,5.315" transform="translate(9.67 9.671)" fill="#e2e2e2"/>
    </g>
  </g>
  <g id="Ellipse_101" data-name="Ellipse 101" transform="translate(245.578 201.989)" fill="none" stroke="rgba(226,226,226,0.37)" stroke-width="2" opacity="0.206">
    <ellipse cx="293.352" cy="293.352" rx="293.352" ry="293.352" stroke="none"/>
    <ellipse cx="293.352" cy="293.352" rx="292.352" ry="292.352" fill="none"/>
  </g>
  <g id="Group_1688" data-name="Group 1688" transform="translate(493 420.98)">
    <g id="Group_1687" data-name="Group 1687" transform="translate(0 0)" clip-path="url(#clip-path-51)">
      <path id="Path_907" data-name="Path 907" d="M59.218,113.179H35.239a2.786,2.786,0,0,1-2.788-2.788A31.059,31.059,0,0,0,19.3,85.283,47.214,47.214,0,1,1,79.825,13.059,46.8,46.8,0,0,1,94.459,47.233,47.287,47.287,0,0,1,74.588,85.688a30.273,30.273,0,0,0-12.582,24.695,2.8,2.8,0,0,1-2.788,2.8m-21.3-5.576h18.62A35.8,35.8,0,0,1,71.359,81.142a41.615,41.615,0,0,0,4.619-64.053A41.241,41.241,0,0,0,45.213,5.623,41.626,41.626,0,0,0,22.6,80.782,36.781,36.781,0,0,1,37.916,107.6" transform="translate(0.003 0)" fill="#ee7b00"/>
      <path id="Path_908" data-name="Path 908" d="M34.561,34.459H10.774a2.788,2.788,0,1,1,0-5.576H34.561a2.788,2.788,0,0,1,0,5.576" transform="translate(24.66 89.185)" fill="#ee7b00"/>
      <path id="Path_909" data-name="Path 909" d="M26.316,37.082H11.923a2.788,2.788,0,1,1,0-5.576H26.316a2.788,2.788,0,1,1,0,5.576" transform="translate(28.208 97.284)" fill="#ee7b00"/>
      <path id="Path_910" data-name="Path 910" d="M16.432,39.675H13.3a2.788,2.788,0,0,1,0-5.576h3.131a2.788,2.788,0,0,1,0,5.576" transform="translate(32.463 105.291)" fill="#ee7b00"/>
      <path id="Path_911" data-name="Path 911" d="M25.565,67.616a2.792,2.792,0,0,1-2.424-4.161L36.549,39.769H9.8a2.782,2.782,0,0,1-2.424-4.161L23.141,7.761a2.789,2.789,0,0,1,4.852,2.751L14.585,34.193H41.332a2.788,2.788,0,0,1,2.424,4.161L27.994,66.2a2.779,2.779,0,0,1-2.428,1.414" transform="translate(21.662 19.605)" fill="#ee7b00"/>
    </g>
  </g>
  <text id="Keeping_your_businesses_moving_in_the_right_direction" data-name="Keeping your businesses moving
in the right direction" transform="translate(973 215.674)" fill="#fff" font-size="36" font-family="Montserrat-Bold, Montserrat" font-weight="700"><tspan x="0" y="0">Keeping </tspan><tspan y="0" fill="#ee7b00" text-decoration="underline">your</tspan><tspan y="0" xml:space="preserve"> businesses moving</tspan><tspan x="0" y="40">in the right direction</tspan></text>
  <text id="Automation_Control_Systems" data-name="Automation &amp; Control Systems" transform="translate(996.5 344.295)" fill="#ee7b00" font-size="26" font-family="Montserrat-Bold, Montserrat" font-weight="700"><tspan x="0" y="0">Automation &amp; Control Systems</tspan></text>
  <text id="Conveyor_Systems" data-name="Conveyor Systems" transform="translate(996.5 427.309)" fill="#ee7b00" font-size="26" font-family="Montserrat-Bold, Montserrat" font-weight="700"><tspan x="0" y="0">Conveyor Systems</tspan></text>
  <text id="Control_Panels" data-name="Control Panels" transform="translate(996.5 513.059)" fill="#ee7b00" font-size="26" font-family="Montserrat-Bold, Montserrat" font-weight="700"><tspan x="0" y="0">Control Panels</tspan></text>
  <text id="PLC_Programming" data-name="PLC Programming" transform="translate(996.5 595.059)" fill="#ee7b00" font-size="26" font-family="Montserrat-Bold, Montserrat" font-weight="700"><tspan x="0" y="0">PLC Programming</tspan></text>
  <text id="Cabling_Infrastructure" data-name="Cabling &amp; Infrastructure" transform="translate(996.5 676.438)" fill="#ee7b00" font-size="26" font-family="Montserrat-Bold, Montserrat" font-weight="700"><tspan x="0" y="0">Cabling &amp; Infrastructure</tspan></text>
  <text id="Mains_Distribution_and_Small_Power" data-name="Mains Distribution and Small Power" transform="translate(996.5 757.815)" fill="#ee7b00" font-size="26" font-family="Montserrat-Bold, Montserrat" font-weight="700"><tspan x="0" y="0">Mains Distribution and Small Power</tspan></text>
  <g id="_" data-name="+" transform="translate(1598.644 409.559)">
    <path id="Union_1" data-name="Union 1" d="M7,16V9H0V7H7V0H9V7h7V9H9v7Z" transform="translate(0.356 0.01)" fill="#fff"/>
  </g>
  <g id="_2" data-name="+" transform="translate(1598.644 327.172)">
    <path id="Union_1-2" data-name="Union 1" d="M7,16V9H0V7H7V0H9V7h7V9H9v7Z" transform="translate(0.356 0.01)" fill="#fff"/>
  </g>
  <g id="_3" data-name="+" transform="translate(1598.644 494.438)">
    <path id="Union_1-3" data-name="Union 1" d="M7,16V9H0V7H7V0H9V7h7V9H9v7Z" fill="#fff"/>
  </g>
  <g id="_4" data-name="+" transform="translate(1598.644 576.438)">
    <path id="Union_1-4" data-name="Union 1" d="M7,16V9H0V7H7V0H9V7h7V9H9v7Z" fill="#fff"/>
  </g>
  <g id="_5" data-name="+" transform="translate(1598.644 657.815)">
    <path id="Union_1-5" data-name="Union 1" d="M7,16V9H0V7H7V0H9V7h7V9H9v7Z" fill="#fff"/>
  </g>
  <g id="_6" data-name="+" transform="translate(1598.644 739.194)">
    <path id="Union_1-6" data-name="Union 1" d="M7,16V9H0V7H7V0H9V7h7V9H9v7Z" fill="#fff"/>
  </g>
  <line id="Line_98" data-name="Line 98" x2="689.144" transform="translate(960 377.682)" fill="none" stroke="#d5d5d5" stroke-width="2"/>
  <line id="Line_94" data-name="Line 94" x2="689.144" transform="translate(960 462.559)" fill="none" stroke="#d5d5d5" stroke-width="2"/>
  <g id="Group_1700" data-name="Group 1700" transform="translate(0 -973.806)">
    <line id="Line_102" data-name="Line 102" x2="689.144" transform="translate(960 1266.611)" fill="none" stroke="#d5d5d5" stroke-width="2"/>
    <line id="Line_103" data-name="Line 103" x2="689.144" transform="translate(960 1351.488)" fill="none" stroke="#d5d5d5" stroke-width="2"/>
  </g>
  <line id="Line_95" data-name="Line 95" x2="689.144" transform="translate(960 544.559)" fill="none" stroke="#d5d5d5" stroke-width="2"/>
  <line id="Line_96" data-name="Line 96" x2="689.144" transform="translate(960 625.938)" fill="none" stroke="#d5d5d5" stroke-width="2"/>
  <line id="Line_92" data-name="Line 92" x2="689.144" transform="translate(960 707.315)" fill="none" stroke="#d5d5d5" stroke-width="2"/>
  <line id="Line_97" data-name="Line 97" x2="689.144" transform="translate(960 625.938)" fill="none" stroke="#d5d5d5" stroke-width="2"/>
  <line id="Line_93" data-name="Line 93" x2="689.144" transform="translate(960 707.315)" fill="none" stroke="#d5d5d5" stroke-width="2"/>
  <line id="Line_91" data-name="Line 91" x2="689.144" transform="translate(960 788.694)" fill="none" stroke="#d5d5d5" stroke-width="2"/>
</svg>

</div>
