<div class="map">

    <div class="container">

        <div class="row">


            <div class="col-md-4 solutions  wow animate__animated animate__fadeIn" data-wow-delay="0s">

                <div class="solutions_content">


                    <sub><?php the_sub_field('sub_title'); ?><span>>></span></sub>
                    <h2><?php the_sub_field('title'); ?></h2>
                    <p><?php the_sub_field('content'); ?></p>

                </div>

            </div>

            <div class="col-md-8 wow animate__animated animate__fadeIn" data-wow-delay="0.25s">

                <div class="map-container">

                <?php 

                // Check rows exists.
                if( have_rows('map_points') ):
                    $i = 0.5;
                    $s = 0.5;
                    // Loop through rows.
                    while( have_rows('map_points') ) : the_row();
                    $i = $i-0.25;

                ?>

                    
                    <img class="pin wow animate__animated animate__fadeIn" data-wow-delay="<?= $i; ?>s"  src="<?= get_template_directory_uri(); ?>/assets/img/pin.svg" alt="map pin" style="position: absolute; left:<?php the_sub_field('x_location'); ?>px; top:<?php the_sub_field('y_location'); ?>px;"/>


                <?php

                    // End loop.
                    endwhile;

                // No value.
                else :
                    // Do something...
                endif;


                ?>

                    <img src="<?= get_template_directory_uri(); ?>/assets/img/map.png"/>
                
                </div>

            </div>


        </div>

    </div>

</div>