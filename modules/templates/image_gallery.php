
<?php 
    $images = get_sub_field('image_gallery');
    $size = 'full'; // (thumbnail, medium, large, full or custom size
    //print_r($images);
    if( $images ): 
?>

<div class="container">
    <div class="row multiple-items">

        <?php 
        $i = 0.5;
        $s = 0.5;
        foreach( $images as $image_id ):      $i = $i+0.25; ?>
            <div class="image  wow animate__animated animate__fadeIn"  data-wow-delay="<?= $i-$s; ?>s">
                <img src="<?= $image_id['url']; ?>" alt="<?= $image_id['alt']; ?>"/>
        </diV>
        <?php endforeach; ?>
    </div> 
</div>

<?php endif; ?>
