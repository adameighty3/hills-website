<?php get_header(); ?>
<div class="flexible-content-blocks">
<div class="hero_block">


<div class="hero_block inner" style="background-image:url('https://hills.local/wp-content/uploads/2022/04/Mask-Group-1.jpg');">
	<div class="hero_block-inner">

	<!-- hero content -->

	<div class="content">
		<div class="container">
			<div class="content-inner">
			
				<div class="hero_content">
					<?php $category = get_category( get_query_var( 'cat' ) );  ?>
					<h1><?= $category->name; ?></h1>
					<a href=""></a>
				</div>
				<sub>Specialist Electrical Contractors</sub>
			</div>
		</div>

	</div>


	</div>
</div>

</div>

<!-- posts -->

<div class="container">

    <div class="row">

        <div class="col-md-8">

            <div class="row">

            <?php
                $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                $query = new WP_Query( array(
                    'posts_per_page' => 6,
                    'paged' => $paged
                ) );
            ?>

            <?php if ( $query->have_posts() ) : ?>

            <!-- begin loop -->
            <?php while ( $query->have_posts() ) : $query->the_post(); ?>

            <div class="col-md-6 cat_post-layout">
                <a href="<?php the_permalink(); ?>">
                <?php echo get_the_post_thumbnail(); ?>
                <h2><?php the_title(); ?></h2>
                <span>
                    <svg xmlns="http://www.w3.org/2000/svg" width="15" height="16.5" viewBox="0 0 15 16.5">
                        <g id="Icon_feather-calendar" data-name="Icon feather-calendar" transform="translate(-3 -1.5)">
                            <path id="Path_831" data-name="Path 831" d="M5.25,4.5h10.5A2.253,2.253,0,0,1,18,6.75v10.5a2.253,2.253,0,0,1-2.25,2.25H5.25A2.253,2.253,0,0,1,3,17.25V6.75A2.253,2.253,0,0,1,5.25,4.5ZM15.75,18a.751.751,0,0,0,.75-.75V6.75A.751.751,0,0,0,15.75,6H5.25a.751.751,0,0,0-.75.75v10.5a.751.751,0,0,0,.75.75Z" transform="translate(0 -1.5)" fill="#ee7b00"/>
                            <path id="Path_832" data-name="Path 832" d="M23.25,6a.75.75,0,0,1-.75-.75v-3a.75.75,0,0,1,1.5,0v3A.75.75,0,0,1,23.25,6Z" transform="translate(-9.75)" fill="#ee7b00"/>
                            <path id="Path_833" data-name="Path 833" d="M11.25,6a.75.75,0,0,1-.75-.75v-3a.75.75,0,0,1,1.5,0v3A.75.75,0,0,1,11.25,6Z" transform="translate(-3.75)" fill="#ee7b00"/>
                            <path id="Path_834" data-name="Path 834" d="M17.25,15H3.75a.75.75,0,0,1,0-1.5h13.5a.75.75,0,0,1,0,1.5Z" transform="translate(0 -6)" fill="#ee7b00"/>
                        </g>
                    </svg>
                    <?php echo get_the_date('dS F Y'); ?>
                </span>
                <?php the_field('sub_content'); ?>
                <a href="<?php the_permalink(); ?>" class="button button-primary">EXPLORE ></a>

                </a>
            </div>

            <?php endwhile; ?>
            <!-- end loop -->


            <div class="pagination">
                <?php 
                    echo paginate_links( array(
                        'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                        'total'        => $query->max_num_pages,
                        'current'      => max( 1, get_query_var( 'paged' ) ),
                        'format'       => '?paged=%#%',
                        'show_all'     => false,
                        'type'         => 'plain',
                        'end_size'     => 2,
                        'mid_size'     => 1,
                        'prev_next'    => true,
                        'prev_text'    => sprintf( '<i></i> %1$s', __( 'Newer Posts', 'text-domain' ) ),
                        'next_text'    => sprintf( '%1$s <i></i>', __( 'Older Posts', 'text-domain' ) ),
                        'add_args'     => false,
                        'add_fragment' => '',
                    ) );
                ?>
            </div>


            <?php wp_reset_postdata(); ?>

            <?php else : ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?>

            </div>
        
        </div>

        <div class="col-md-4">

            <!-- filter by cat -->
            <div class="category-filter">

                <?php $categories = get_categories(); ?>

                <label>Filter By</label>
                
                <select class="select_category">
                    <?php foreach($categories as $category){ ?>

                        <option value="<?= $category->name; ?>">
                            <?= $category->name; ?>
                        </option>

                    <?php } ?>
                </select>

            </div>

            <!--popular posts -->
            <div class="popular-posts">

                <h2>Popular Posts <span>>></span></h2>
                <div class="container no-gutters">
                
                <?php 
                $popularpost = new WP_Query( array( 'posts_per_page' => 4, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC'  ) );
                while ( $popularpost->have_posts() ) : $popularpost->the_post();
                
                ?>

                <a href="<?php the_permalink(); ?>" class="row">

                    <div class="col-md-5">
                         <?php echo get_the_post_thumbnail(); ?>
                    </div>
                    <div class="col-md-7">

                        <h3><?php the_title(); ?></h3>
                        <span>
                    <svg xmlns="http://www.w3.org/2000/svg" width="15" height="16.5" viewBox="0 0 15 16.5">
                        <g id="Icon_feather-calendar" data-name="Icon feather-calendar" transform="translate(-3 -1.5)">
                            <path id="Path_831" data-name="Path 831" d="M5.25,4.5h10.5A2.253,2.253,0,0,1,18,6.75v10.5a2.253,2.253,0,0,1-2.25,2.25H5.25A2.253,2.253,0,0,1,3,17.25V6.75A2.253,2.253,0,0,1,5.25,4.5ZM15.75,18a.751.751,0,0,0,.75-.75V6.75A.751.751,0,0,0,15.75,6H5.25a.751.751,0,0,0-.75.75v10.5a.751.751,0,0,0,.75.75Z" transform="translate(0 -1.5)" fill="#ee7b00"/>
                            <path id="Path_832" data-name="Path 832" d="M23.25,6a.75.75,0,0,1-.75-.75v-3a.75.75,0,0,1,1.5,0v3A.75.75,0,0,1,23.25,6Z" transform="translate(-9.75)" fill="#ee7b00"/>
                            <path id="Path_833" data-name="Path 833" d="M11.25,6a.75.75,0,0,1-.75-.75v-3a.75.75,0,0,1,1.5,0v3A.75.75,0,0,1,11.25,6Z" transform="translate(-3.75)" fill="#ee7b00"/>
                            <path id="Path_834" data-name="Path 834" d="M17.25,15H3.75a.75.75,0,0,1,0-1.5h13.5a.75.75,0,0,1,0,1.5Z" transform="translate(0 -6)" fill="#ee7b00"/>
                        </g>
                    </svg>
                    <?php echo get_the_date('dS F Y'); ?>
                </span>
                        
                        <button class="button no-button">
                            READ MORE
                            <svg xmlns="http://www.w3.org/2000/svg" width="18.506" height="12.343" viewBox="0 0 18.506 12.343">
                                <path id="Icon_ionic-ios-arrow-round-forward" data-name="Icon ionic-ios-arrow-round-forward" d="M19.677,11.488a.84.84,0,0,0-.006,1.183l3.908,3.915H8.7a.836.836,0,0,0,0,1.671H23.572l-3.908,3.915a.846.846,0,0,0,.006,1.183.832.832,0,0,0,1.176-.006l5.3-5.335h0a.938.938,0,0,0,.174-.264.8.8,0,0,0,.064-.321.838.838,0,0,0-.238-.585l-5.3-5.335A.819.819,0,0,0,19.677,11.488Z" transform="translate(-7.875 -11.252)" fill="#ee7b00"/>
                            </svg>
                        </button>  

                    </div>
                
                </a>

                <?php wp_reset_postdata(); ?>
                <?php
                endwhile;
                ?>
                </div>
            </div>

            <!-- Need Help Form -->

            <div class="need_help-form">

                <h2>Need help?</h2>
                <p>Feel free to fill out our form below and a member of our team will be in touch.</p>

                <?php echo do_shortcode('[forminator_form id="165"]'); ?>
            </div>


        </div>
    </div>
</div>
            </div>
<?php get_footer(); ?>
