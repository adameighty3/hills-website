<?php
// Enqueue scripts & styles
function enqueue_scripts_and_styles() {
    wp_enqueue_style( 'style', get_template_directory_uri() . '/assets/dist/main.css' );
    wp_enqueue_script( 'custom', get_template_directory_uri() . '/assets/dist/all.js', array(), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'enqueue_scripts_and_styles' );
//


//Remove Gutenberg Block Library CSS from loading on the frontend
function smartwp_remove_wp_block_library_css(){
    wp_dequeue_style( 'wp-block-library' );
    wp_dequeue_style( 'wp-block-library-theme' );
    wp_dequeue_style( 'wc-block-style' ); // Remove WooCommerce block CSS
}
add_action( 'wp_enqueue_scripts', 'smartwp_remove_wp_block_library_css', 100 );
add_filter( 'jetpack_implode_frontend_css', '__return_false', 99 );

function remove_wp_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'remove_wp_ver_css_js', 9999 );
add_filter( 'script_loader_src', 'remove_wp_ver_css_js', 9999 );