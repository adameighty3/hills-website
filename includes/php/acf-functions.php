<?php

// Adding Options Pages
    if( function_exists('acf_add_options_page') ) {

    	acf_add_options_page(array(
    		'page_title' 	=> 'Site Settings',
    		'menu_title'	=> 'Site Settings',
    		'menu_slug' 	=> 'site-settings',
    		'capability'	=> 'edit_posts',
            'icon_url'      => 'dashicons-menu-alt3',
    		'redirect'		=> false
    	));

    }
//

// Initiliase Google Maps API for ACF map
function my_acf_init() {
    acf_update_setting('google_api_key', 'AIzaSyAyeHZmrpJw3hZtdnu1HAgdK2wuKHoOgR4');
}
add_action('acf/init', 'my_acf_init');
//
