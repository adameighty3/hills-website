<?php

// Create Case Studies Post Type
function create_case_studies() {
    register_post_type( 'case_studies',
        array(
          'labels' => array(
          'name' => __( 'Case Studies' ),
          'singular_name' => __( 'Case Study' ),
          'menu_name'   => __( 'Case Studies', 'Admin Menu text', 'textdomain' ),
          'all_items'             => __( 'Case Studies', 'textdomain' ),
          'name_admin_bar'        => _x( 'Case Studies', 'Add New on Toolbar', 'textdomain' ),
          'add_new'               => __( 'Create New', 'textdomain' ),
          'add_new_item'          => __( 'New Case Study', 'textdomain' ),
          'new_item'              => __( 'New Case Study', 'textdomain' ),
          'edit_item'             => __( 'Edit Case Study', 'textdomain' ),
          'view_item'             => __( 'View Case Study', 'textdomain' ),
          'menu_position'    => 10,
          ),
          'has_archive'       => true,
          'public' => true,
          'menu_icon' =>  'dashicons-star-filled'
        )
    );
}
add_action( 'init', 'create_case_studies' );


//hook into the init action and call create_book_taxonomies when it fires
 
add_action( 'init', 'create_subjects_hierarchical_taxonomy', 0 );
 
//create a custom taxonomy name it subjects for your posts
 
function create_subjects_hierarchical_taxonomy() {
 
// Add new taxonomy, make it hierarchical like categories
//first do the translations part for GUI
 
  $labels = array(
    'name' => _x( 'Types', 'taxonomy general name' ),
    'singular_name' => _x( 'Type', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Types' ),
    'all_items' => __( 'All Types' ),
    'parent_item' => __( 'Parent Type' ),
    'parent_item_colon' => __( 'Parent Type:' ),
    'edit_item' => __( 'Edit Type' ), 
    'update_item' => __( 'Update Type' ),
    'add_new_item' => __( 'Add New Type' ),
    'new_item_name' => __( 'New Type Name' ),
    'menu_name' => __( 'Types' ),
  );    
 
// Now register the taxonomy
  register_taxonomy('types',array('case_studies'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_in_rest' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'type' ),
  ));
 
}