<?php

// Get strings at custom lengths with break on word
function truncate_str($str, $maxlen) {
if ( strlen($str) <= $maxlen ) return $str;

$newstr = substr($str, 0, $maxlen);
if ( substr($newstr,-1,1) != ' ' ) $newstr = substr($newstr, 0, strrpos($newstr, " "));

return $newstr;
}
//

// Output Site Settings Contact Number with shortcode
add_shortcode('site_contact_number', 'output_site_contact_number');
function output_site_contact_number() {
  return '<a class="site-contact-number" href="tel:' . get_field('phone_number', 'options') . '">' . get_field('phone_number', 'options') . '</a>';
}
//

// Output Site Settings Email Address with shortcode
add_shortcode('site_email_address', 'output_site_email_address');
function output_site_email_address() {
  return '<a class="site-email-address" href="mailto:' . get_field('email_address', 'options') . '">' . get_field('email_address', 'options') . '</a>';
}
//

add_theme_support( 'post-thumbnails' );

//Adding support for popular posts
function wpb_set_post_views($postID) {
  $count_key = 'wpb_post_views_count';
  $count = get_post_meta($postID, $count_key, true);
  if($count==''){
      $count = 0;
      delete_post_meta($postID, $count_key);
      add_post_meta($postID, $count_key, '0');
  }else{
      $count++;
      update_post_meta($postID, $count_key, $count);
  }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

function wpb_track_post_views ($post_id) {
  if ( !is_single() ) return;
  if ( empty ( $post_id) ) {
      global $post;
      $post_id = $post->ID;    
  }
  wpb_set_post_views($post_id);
}
add_action( 'wp_head', 'wpb_track_post_views');