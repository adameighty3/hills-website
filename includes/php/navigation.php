<?php
register_nav_menus( array(
	'primary' => esc_html__( 'Primary Navigation', 'supertheme' ),
	'footer' => esc_html__( 'Footer Navigation', 'supertheme' ),
) );
