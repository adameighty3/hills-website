<?php
// Flexible Content - build blocks based on ACF custom field modules
function flexibleContent() {
    // if we don't have any rows, return nothing
    if( have_rows('flexible_content') == false ) return;
    //while we have rows...
    while ( have_rows('flexible_content') ) : the_row();
        // include the template for that module
        checkAndInclude( __DIR__ . '/../../modules/templates/' . get_row_layout() . '.php' );
    endwhile;
}

// Throw an error if module template is not found
function checkAndInclude($filename) {
    if (file_exists($filename) === false) {
        return;
        throw new TemplateNotFoundException($filename . ' was not found.');
    }
// or return it if found
    return include $filename;
}