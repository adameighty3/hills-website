<?php

include __DIR__ . '/includes/php/acf-functions.php';
include __DIR__ . '/includes/php/custom-post-types.php';
include __DIR__ . '/includes/php/flexible-content-looper.php';
include __DIR__ . '/includes/php/generic-functions.php';
include __DIR__ . '/includes/php/navigation.php';
include __DIR__ . '/includes/php/scripts-styles.php';
