<?php get_header();?>

<div class="container-fluid hero_block">
	<div class="row">
		<div class="container">
			<h1>Welcome to our Blog</h1>
		</div>
	</div>
</div>

<?php 
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$args = [
	'post_type' => 'post',
	'post_status' => 'publish',
    'posts_per_page' => 8,
    'paged' => $paged,
];
$the_query = new WP_Query($args);
?>
<?php if( $the_query->have_posts()) : ?>

	<div class="container blog-posts">
		<div class="row">

			<?php while($the_query->have_posts()) : $the_query->the_post(); ?>
			
				<a class="col-12 permalink" href="<?php echo get_the_permalink();?>">
					<div class="blog-post">
						<div class="wrapper">						
							<div class="row">					
								<div class="col-4 blog-img">
									<img src="<?php echo get_the_post_thumbnail_url();?>" alt="">
								</div>
								<div class="col-8 blog-content">					
									<div class="wrapper">
										<h3><?php the_title();?></h3>
										<p><?php echo truncate_str(strip_tags(get_the_content()), 400) . '&hellip;';?></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</a>			
			<?php endwhile; ?>

			<div class="col-12">				
				<div class="pagination">
				    <?php echo paginate_links( array(
				        'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
				        'total'        => $the_query->max_num_pages,
				        'current'      => max( 1, get_query_var( 'paged' ) ),
				        'format'       => '?paged=%#%',
				        'show_all'     => false,
				        'type'         => 'plain',
				        'end_size'     => 2,
				        'mid_size'     => 1,
				        'prev_next'    => true,
				        'prev_text'    => sprintf( '<i></i> %1$s', __( '&lt;', 'text-domain' ) ),
				        'next_text'    => sprintf( '%1$s <i></i>', __( '&gt;', 'text-domain' ) ),
				        'add_args'     => false,
				        'add_fragment' => '',
				    ) ); ?>
				</div>
			</div>

			<?php wp_reset_postdata(); ?>
		
		</div>
	</div>

<?php endif; ?>

<?php get_footer();?>