<?php get_header();?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<!-- Modules -->
<div class="flexible-content-blocks">
<?php get_template_part( 'modules/flexible', 'content' ); ?>
</div>

<?php endwhile; else: ?>
<?php endif; ?>

<?php get_footer();?>