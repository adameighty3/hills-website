
<div class="container">
	<div class="footer-content">

		<h2>Get in Touch</h2>
		<p>Get in touch to speak to one of our engineering experts</p>
		
		<div class="contact-details">
		
			<div class="phone">

				<svg xmlns="http://www.w3.org/2000/svg" width="37.25" height="37.25" viewBox="0 0 37.25 37.25">
				<g id="Component_21_1" data-name="Component 21 – 1" transform="translate(1 1)">
					<circle id="Ellipse_60" data-name="Ellipse 60" cx="17.625" cy="17.625" r="17.625" fill="none" stroke="#fff" stroke-width="2"/>
					<path id="Icon_feather-phone" data-name="Icon feather-phone" d="M14.479,16.373c-.061,0-.123,0-.183-.008h-.012a14.171,14.171,0,0,1-6.147-2.186A14,14,0,0,1,3.863,9.9,14.169,14.169,0,0,1,1.677,3.728V3.718A2.035,2.035,0,0,1,3.7,1.5H5.75A2.035,2.035,0,0,1,7.764,3.25v.006A8.043,8.043,0,0,0,8.2,5.014a2.04,2.04,0,0,1-.458,2.145l0,0-.486.486a10.182,10.182,0,0,0,3.11,3.11l.486-.486,0,0A2.035,2.035,0,0,1,13,9.812a8.039,8.039,0,0,0,1.756.437h.006a2.035,2.035,0,0,1,1.75,2.056v2.026a2.035,2.035,0,0,1-2.035,2.042Zm-.056-1.359.056,0a.678.678,0,0,0,.678-.681s0,0,0,0V12.3q0-.008,0-.017a.678.678,0,0,0-.581-.688,9.4,9.4,0,0,1-2.051-.511.678.678,0,0,0-.716.151l-.86.86a.678.678,0,0,1-.815.11A11.536,11.536,0,0,1,5.811,7.879a.678.678,0,0,1,.11-.815l.86-.86a.68.68,0,0,0,.152-.715,9.4,9.4,0,0,1-.512-2.052.681.681,0,0,0-.671-.581H3.7a.678.678,0,0,0-.675.735A12.806,12.806,0,0,0,5,9.169l0,.005a12.63,12.63,0,0,0,3.861,3.861l.005,0A12.808,12.808,0,0,0,14.423,15.014Z" transform="translate(9.249 9.127)" fill="#fff"/>
				</g>
				</svg>

				<a href="tel:<?php the_field('phone_number', 'options'); ?>"><?php the_field('phone_number', 'options'); ?></a>

			</div>

			<div class="email">

			<svg xmlns="http://www.w3.org/2000/svg" width="37.25" height="37.25" viewBox="0 0 37.25 37.25">
			<g id="Component_22_1" data-name="Component 22 – 1" transform="translate(1 1)">
				<circle id="Ellipse_61" data-name="Ellipse 61" cx="17.625" cy="17.625" r="17.625" fill="none" stroke="#fff" stroke-width="2"/>
				<path id="Icon_zocial-email" data-name="Icon zocial-email" d="M.072,15.187V5.149q0-.017.052-.331L6.033,9.872.142,15.536a1.477,1.477,0,0,1-.07-.349ZM.856,4.12a.751.751,0,0,1,.3-.052H17.065a.987.987,0,0,1,.314.052L11.453,9.192l-.784.627L9.117,11.092,7.566,9.819l-.784-.627Zm.017,12.1,5.943-5.7,2.3,1.865,2.3-1.865,5.943,5.7a.837.837,0,0,1-.3.052H1.153a.789.789,0,0,1-.279-.052ZM12.2,9.872l5.891-5.054a1.04,1.04,0,0,1,.052.331V15.187a1.336,1.336,0,0,1-.052.349Z" transform="translate(8.428 7.432)" fill="#fff"/>
			</g>
			</svg>

			<a href="mailto:<?php the_field('email_address', 'options'); ?>"><?php the_field('email_address', 'options'); ?></a>



			</div>

		</div>

	</div>
</div>


<footer>
	<div class="container">
		<div class="row">
			<div class="col-4">
				<div class="row">
					<div class="col-md-6">
						<div class="footer-constants">
							<a href="/">
								<img class="footer-logo" src="<?php echo get_field('footer_logo','options')['url']?>" alt="<?php echo get_field('footer_logo','options')['url']?>"/>
							</a>
						</div>
					</div>
						<div class="col-md-6">
							<p>
								<?php $count = count(get_field('address','options'));
								if(have_rows('address', 'options')) : $n=1; while(have_rows('address', 'options')) : the_row(); ?>
									<?php
									if($n < $count) { echo '<span>'. get_sub_field('line') . ',</span> '; }
									else { the_sub_field('line'); }
								$n++; endwhile; endif; ?>
							</p>
						</div>
					</div>
				</div>
			
			<div class="col-6">
				<div class="row">
					<div class="col-md-4">
						<p>Registered No: XXXXXXXX</p>
						<p>VAT No: XXXXXXXXX</p>
					</div>
					<div class="col-md-4">

						<?php wp_nav_menu( array(
							'theme_location' => 'footer', // Defined when registering the menu
							'menu_id'        => 'footer-navigation',
							'container'      => 'div',
						)); ?>

					</div>
					<div class="col-md-4">

						<img src="<?= get_template_directory_uri(); ?>/assets/img/logos.png"/>

					</div>
			</div>
		</div>
	</div>

</footer>


<div class="credits">

	<div class="container">
		<div class="credits-inner">
			<div>
				© HILLS ELECTRICAL CONTRACTORS LTD TRADING AS HILLS GROUP 2022
			</div>
			<div>
				Website by: <a href="https://eighty3creative.co.uk" target="_blank">eighty3</a>
			</div>
		</div>
	</div>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<?php wp_footer();?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js" integrity="sha512-XtmMtDEcNz2j7ekrtHvOVR4iwwaD6o/FUJe6+Zq+HgcCsk3kj4uSQQR8weQ2QVj1o0Pk6PwYLohm206ZzNfubg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
	jQuery('.multiple-items').slick({
	  infinite: true,
	  slidesToShow: 7,
	  slidesToScroll: 1,
	  settings: {
		arrows: true,
	  }
	});


	jQuery('.slicksliders').slick({
	  infinite: true,
	  slidesToShow: 3,
	  slidesToScroll: 1,
	   centerMode: true,
  variableWidth: true, 
	  settings: {
		arrows: false,
	  }
	});

	new WOW().init();

	
</script>

</body>
</html>
