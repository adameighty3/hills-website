<html>
	<head>
		<?php wp_head(); ?>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title><?php wp_title(); ?></title>
		<link rel="preconnect" href="https://fonts.googleapis.com">
		<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.css" integrity="sha512-wR4oNhLBHf7smjy0K4oqzdWumd+r5/+6QO/vDda76MW5iug4PT7v86FoEkySIJft3XA0Ae6axhIvHrqwm793Nw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.css" integrity="sha512-6lLUdeQ5uheMFbWm3CP271l14RsX1xtx+J5x2yeIDkkiBpeVTNhTqijME7GgRKKi6hCqovwCoBTlRBEC20M8Mg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	</head>

<body <?php body_class('animate__animated  animate__fadeIn');?>>
	<?php wp_body_open();?>
	<header>
		<div class="container">
			<a href="/">
				<img class="header-logo" src="<?php echo get_field('header_logo','options')['url']?>" alt="<?php echo get_field('header_logo','options')['url']?>"/>
			</a>
			<div class="right-container">
			<div class="nav">

			<?php wp_nav_menu( array(
                'theme_location' => 'primary', // Defined when registering the menu
                'menu_id'        => 'primary-navigation',
                'container'      => 'div',
            )); ?>

			</div>

			<div class="icons">
				<div class="search-icon">
					<input class="form-control" placeholder="Enter your search term..."/>	
					<svg id="Group_250" data-name="Group 250" xmlns="http://www.w3.org/2000/svg" width="35" height="35" viewBox="0 0 35 35">
							<rect id="Rectangle_693" data-name="Rectangle 693" width="35" height="35" fill="none"/>
						<path id="Path_208" data-name="Path 208" d="M34.255,31.239l-7.11-7.11a14.751,14.751,0,0,0,3.016-9.049A14.988,14.988,0,0,0,15.081,0,14.988,14.988,0,0,0,0,15.081,14.988,14.988,0,0,0,15.081,30.162a14.751,14.751,0,0,0,9.049-3.016l7.11,7.11ZM4.309,15.081A10.668,10.668,0,0,1,15.081,4.309,10.668,10.668,0,0,1,25.853,15.081,10.668,10.668,0,0,1,15.081,25.853,10.668,10.668,0,0,1,4.309,15.081Z" fill="#ee7b00"/>
					</svg>
				</div>

				<div class="phone-icon">
				<div class="show-phone">
				<a href="tel:<?php the_field('phone_number','options'); ?>"><strong>Call us on:</strong> <?php the_field('phone_number','options'); ?></a>
				</div>
				<svg xmlns="http://www.w3.org/2000/svg" width="37.25" height="37.25" viewBox="0 0 37.25 37.25">
					<g id="Component_23_1" data-name="Component 23 – 1" transform="translate(1 1)">
						<circle id="Ellipse_60" data-name="Ellipse 60" cx="17.625" cy="17.625" r="17.625" fill="none" stroke="#ee7b00" stroke-width="2"/>
						<path id="Icon_feather-phone" data-name="Icon feather-phone" d="M14.479,16.373c-.061,0-.123,0-.183-.008h-.012a14.171,14.171,0,0,1-6.147-2.186A14,14,0,0,1,3.863,9.9,14.169,14.169,0,0,1,1.677,3.728V3.718A2.035,2.035,0,0,1,3.7,1.5H5.75A2.035,2.035,0,0,1,7.764,3.25v.006A8.043,8.043,0,0,0,8.2,5.014a2.04,2.04,0,0,1-.458,2.145l0,0-.486.486a10.182,10.182,0,0,0,3.11,3.11l.486-.486,0,0A2.035,2.035,0,0,1,13,9.812a8.039,8.039,0,0,0,1.756.437h.006a2.035,2.035,0,0,1,1.75,2.056v2.026a2.035,2.035,0,0,1-2.035,2.042Zm-.056-1.359.056,0a.678.678,0,0,0,.678-.681s0,0,0,0V12.3q0-.008,0-.017a.678.678,0,0,0-.581-.688,9.4,9.4,0,0,1-2.051-.511.678.678,0,0,0-.716.151l-.86.86a.678.678,0,0,1-.815.11A11.536,11.536,0,0,1,5.811,7.879a.678.678,0,0,1,.11-.815l.86-.86a.68.68,0,0,0,.152-.715,9.4,9.4,0,0,1-.512-2.052.681.681,0,0,0-.671-.581H3.7a.678.678,0,0,0-.675.735A12.806,12.806,0,0,0,5,9.169l0,.005a12.63,12.63,0,0,0,3.861,3.861l.005,0A12.808,12.808,0,0,0,14.423,15.014Z" transform="translate(9.249 9.127)" fill="#ee7b00"/>
					</g>
				</svg>


				</div>

				<div class="linkedin">
					<a href="/">
						<svg class="search" xmlns="http://www.w3.org/2000/svg" width="37.25" height="37.25" viewBox="0 0 37.25 37.25">
							<g id="Group_1545" data-name="Group 1545" transform="translate(-1655.5 90.25)">
								<circle id="Ellipse_60" data-name="Ellipse 60" cx="17.625" cy="17.625" r="17.625" transform="translate(1656.5 -89.25)" fill="none" stroke="#ee7b00" stroke-width="2"/>
								<rect id="Rectangle_1128" data-name="Rectangle 1128" width="3.898" height="11.718" transform="translate(1665.607 -75.46)" fill="#ee7b00"/>
								<path id="Path_260" data-name="Path 260" d="M1402.229,23.051h-.026a2.225,2.225,0,1,1,.026,0Z" transform="translate(265.329 -100.115)" fill="#ee7b00" fill-rule="evenodd"/>
								<path id="Path_261" data-name="Path 261" d="M1416.575,34.962h-3.9v-6.27c0-1.575-.565-2.649-1.974-2.649a2.134,2.134,0,0,0-2,1.429,2.624,2.624,0,0,0-.126.945v6.546h-3.9s.046-10.624,0-11.718h3.9V24.9a3.856,3.856,0,0,1,3.51-1.937c2.566,0,4.486,1.677,4.486,5.277Z" transform="translate(266.979 -98.705)" fill="#ee7b00" fill-rule="evenodd"/>
							</g>
						</svg>
					</a>

				</div>

				</div>
			</div>
		</div>
	</header>
